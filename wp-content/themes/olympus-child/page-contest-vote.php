<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package olympus
 */
$olympus = Olympus_Options::get_instance();
$post_style = $olympus->get_option_final('blog_style', 'classic', array('final-source' => 'customizer'));
$layout = olympus_sidebar_conf($post_style === 'list' ? true : false);
$main_class = 'full' !== $layout['position'] ? 'site-main content-main-sidebar' : 'site-main content-main-full';
$container_width = 'container';
$ajax_blog_panel = olympus_get_post_sort_panel();
acf_form_head();
$contest = get_post($_GET['contest']);
$contest_items = GFCore_Contest_Entries::get_contest_entry_items($contest);
$user_id = get_current_user_id();
$users_vote_count = get_user_meta($user_id, 'vote_power', true);

$terms = get_the_terms( $contest->ID, 'contest_status' );
if ( !empty( $terms ) ){
    $term = array_shift( $terms );
    $contest_status = $term->slug;
}

if ($contest_status != 'open-contests') {
    wp_redirect( get_home_url() . '/contests/open-contests' );
    exit;
}

acf_form_head();
?>

<?php get_header(); ?>
   <?php get_template_part('template-parts/vote/content', 'single-vote-item-header'); ?>

    <div id="vote-page" class="<?php echo esc_attr($container_width) ?>">
        <div class="row primary-content-wrapper">
            <div class="col-lg-12">
                <?php olympus_render($ajax_blog_panel); ?>
            </div>
            <div class="<?php echo esc_attr($layout['content-classes']) ?>">
                <main id="main" class="<?php echo esc_attr($main_class) ?> loading-items">

                    <?php if(!empty($contest_items)): ?>

                        <div class="masonry-wrapper">
                            <div class="masonry">
                                <?php
                                $index = 1;
                                foreach ($contest_items as $contest_item): ?>
                                    <?php if ($contest_item['image']): ?>
                                    <div class="masonry-item">
                                    <div id="<?php echo $index; ?>" class="masonry-item-inner"
                                        data-contest-entry-id="<?php echo $contest_item['entry_id']; ?>"
                                        data-contest-entry-row="<?php echo $contest_item['image_row_key']; ?>"
                                        data-contest-photo-id="<?php echo $index; ?>"
                                        data-image="<?php echo $contest_item['image']; ?>"
                                        data-exposure="<?php echo $contest_item['exposure']; ?>"
                                    >
                                            <span class="vote-count">
                                                <?php echo $users_vote_count; ?>
                                            </span>
                                            <img src="<?php echo $contest_item['image']; ?>"
                                                class="masonry-content"
                                                alt="<?php echo $index; ?>"
                                            />
                                    </div>
                                       <a
                                             class="masonry-item__trigger"
                                             href="<?php echo $contest_item['image']; ?>"
                                                data-fancybox="gallery">
                                             </a>
                                        </div>
                                    <?php endif; ?>
                                <?php
                                $index++;
                                endforeach;?>
                            </div>
                        </div>
                    <?php else: ?>
                        <?php echo __(`There are no photo's yet`,`olympus-child`); ?>
                    <?php endif; ?>
                </main><!-- #main -->
            </div>
        </div><!-- #row -->
        <div class="row justify-content-center text-center">
            <div class="col-12">
               <button id="submit-votes" class="primary-button submit-votes-button submit-votes-button--lower">
                  <span>
                      <?php echo __('Submit votes','olympus-child') ?>
                  </span>
               </button>
            </div>
        </div>
    </div><!-- #primary -->

     <div class="hidden-form">

        <?php

        acf_form(array(
            'post_id'		=> 'new_post',
            'post_title'	=> false,
            'post_content'	=> false,
            'new_post'		=> array(
                'post_type'		=> 'votes',
                'post_status'	=> 'publish'
            ),
            'return'		=> get_permalink($contest),
            'submit_value'	=> 'Submit'
        ));

        ?>

    </div>

<?php get_footer(); ?>
