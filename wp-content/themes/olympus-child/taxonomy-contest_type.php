<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package olympus
 */
$olympus         = Olympus_Options::get_instance();
$post_style      = $olympus->get_option_final( 'blog_style', 'classic', array('final-source' => 'customizer') );
$layout          = olympus_sidebar_conf( $post_style === 'list' ? true : false );
$main_class      = 'full' !== $layout[ 'position' ] ? 'site-main content-main-sidebar' : 'site-main content-main-full';
$container_width = 'container';
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/contest-status/content', 'contest-status-header'); ?>
    <div id="primary" class="<?php echo esc_attr( $container_width ) ?>">
        <div class="row primary-content-wrapper">
            <div class="<?php echo esc_attr( $layout[ 'content-classes' ] ) ?>">
                <main id="main" class="<?php echo esc_attr( $main_class ) ?>">
                    <div class="row">
                        <?php
                        if ( have_posts() ) :
                            while ( have_posts() ) : the_post();
                                get_template_part('template-parts/contest/content', 'single-contest-card');
                            endwhile;
                        endif;
                        ?>
                    </div>
                </main><!-- #main -->
            </div>
        </div><!-- #row -->
    </div><!-- #primary -->
<?php get_template_part('template-parts/common/content', 'booster-modal'); ?>
<?php get_footer(); ?>