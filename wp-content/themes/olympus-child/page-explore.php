<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package olympus
 */
$olympus = Olympus_Options::get_instance();
$post_style = $olympus->get_option_final('blog_style', 'classic', array('final-source' => 'customizer'));
$layout = olympus_sidebar_conf($post_style === 'list' ? true : false);
$main_class = 'full' !== $layout['position'] ? 'site-main content-main-sidebar' : 'site-main content-main-full';
$container_width = 'container';
$ajax_blog_panel = olympus_get_post_sort_panel();
acf_form_head();
$contest = get_post($_GET['contest']);
$winner_items = get_field('winner_gallery','option');

print_r($winner_items);

$size = 'large';
?>

<?php get_header(); ?>

    <?php get_template_part('template-parts/common/content', 'gallery-header'); ?>

    <div id="winners-page" class="<?php echo esc_attr($container_width) ?>">
        <div class="row primary-content-wrapper">
            <div class="col-lg-12">
                <?php olympus_render($ajax_blog_panel); ?>
            </div>
            <div class="<?php echo esc_attr($layout['content-classes']) ?>">
                <main id="main" class="<?php echo esc_attr($main_class) ?>">

                    <?php
                     if(!empty($winner_items)): ?>

                        <div class="masonry-wrapper">
                            <div class="masonry">
                                <?php foreach ($winner_items as $index => $winner_item): ?>
                                    <div id="<?php echo $index; ?>" class="masonry-item"
                                        data-image="<?php echo $winner_item['image']; ?>"
                                    >
                                            <img src="<?php echo wp_get_attachment_image_url( $winner_item, $size ); ?>"
                                                class="masonry-content"
                                                alt="<?php echo $index; ?>"
                                            />
                                    </div>

                                <?php endforeach;?>
                            </div>
                        </div>


                    <?php else: ?>
                        <?php echo __(`There are no photo's yet`,`olympus-child`); ?>
                    <?php endif; ?>
                </main><!-- #main -->
            </div>
        </div><!-- #row -->

    </div><!-- #primary -->

<?php get_footer(); ?>
