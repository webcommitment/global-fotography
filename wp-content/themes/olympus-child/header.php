<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<?php wp_head(); ?>

    <?php get_template_part('template-parts/common/content', 'social-share-ranking-head'); ?>
</head>
	<body <?php body_class(); ?> itemscope="itemscope" itemtype="https://schema.org/WebPage">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/nl_NL/sdk.js#xfbml=1&version=v10.0&appId=499731678085183" nonce="XvK4rQuD"></script>
	<?php wp_body_open(); ?>
	<div id="overflow-x-wrapper"  x-data="olympusModal()" @keydown.escape="modalClose()">
<?php
        /**
         * Hook: crumina_body_start.
         *
         * @hooked _action_olympus_tracking_scripts - 10
         * @hooked _action_olympus_add_left_panel - 10
         * @hooked _action_olympus_add_header - 10
         */
        do_action( 'crumina_body_start' );
	?>

<div id="content" class="site-content hfeed site">