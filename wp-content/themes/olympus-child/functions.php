<?php

/* -------------------------------------------------------
 Enqueue CSS from child theme style.css
-------------------------------------------------------- */


function crum_child_css()
{
    wp_enqueue_style('child-style', get_stylesheet_uri());
}

add_action('wp_enqueue_scripts', 'crum_child_css', 99);


/* -------------------------------------------------------
 You can add your custom functions below
-------------------------------------------------------- */
function webcommitment_starter_scripts()
{
    wp_enqueue_script('webcommitment-theme-script', get_template_directory_uri() . '-child/js/webcommitment.js', array('jquery'), null);
    wp_enqueue_script('webcommitment-toasts-script', get_template_directory_uri() . '-child/node_modules/jquery-toast-plugin/dist/jquery.toast.min.js', array('jquery'), null);
    wp_enqueue_style('webcommitment-theme-style', get_template_directory_uri() . '-child/dist/css/style.css');
    wp_enqueue_style('webcommitment-toast-style', get_template_directory_uri() . '-child/node_modules/jquery-toast-plugin/dist/jquery.toast.min.css');

    wp_enqueue_style('webcommitment-fancybox-style', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css');
    wp_enqueue_script('webcommitment-fancybox-script', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js', array('jquery'), null);
}

add_action('wp_enqueue_scripts', 'webcommitment_starter_scripts');

function custom_yz_badges_shortcode($atts)
{
    $default = array(
        'link' => '#',
        'user' => 0,
    );
    $a = shortcode_atts($default, $atts);

    $badges = mycred_get_users_badges(get_current_user_id());

    $html = '
     <div class="yz-main-column grid-column">
        <div class="yz-widget yz-custom_infos yz_effect yz-white-bg fadeIn full-visible" data-effect="fadeIn">
            <div class="yz-widget-main-content">
               <div class="yz-widget-head">
                  <h2 class="yz-widget-title">
                     <i class="fas fa-trophy"></i>Badges					
                  </h2>
               </div>
               <div class="yz-widget-content">
                  <div class="yz-infos-content badges-content">';
    foreach ($badges as $index => $badge) :

        $badge = mycred_get_badge($index);

        $html .= $badge->main_image;
    endforeach;
    wp_reset_query();
    $html .= '   </div>
               </div>
            </div>
        </div>
     </div>
    ';

    return $html;
}

add_shortcode('badges_shortcode', 'custom_yz_badges_shortcode');

function custom_yz_contests_shortcode()
{
    $contest_entries = GFCore_Contest_Entries::get_users_open_contest_entries();
    if ($contest_entries->have_posts()) : ?>
        <h4 class="my-contests-header"><?php echo __('Open contests', 'olympus-child') ?></h4>
        <?php
        $i = 1;
        while ($contest_entries->have_posts()) : $contest_entries->the_post(); ?>
            <div class="single-contest-card">
                <div class="single-contest-card__inner"
                     href="<?php echo get_permalink(); ?>"
                >
                    <?php
                        get_template_part('template-parts/common/content', 'single-contest-entry');
                    ?>
                </div>
            </div>
        <?php
            $i++;
        endwhile; ?>
    <?php endif;
}

add_shortcode('contests_shortcode', 'custom_yz_contests_shortcode');

function custom_yz_past_contests_shortcode()
{
    $contest_entries = GFCore_Contest_Entries::get_users_past_contests();
    $bg_image = GFCore_Contests::get_stunning_header_background_url(get_the_ID());
    if ($contest_entries->have_posts()) : ?>

        <h4 class="my-contests-header"><?php echo __('Closed contests', 'olympus-child') ?></h4>
        <div class="row">
            <?php while ($contest_entries->have_posts()) : $contest_entries->the_post(); ?>
                <div class="single-contest-card col-md-6">
                    <a class="single-contest-card__inner"
                       href="<?php echo get_permalink(); ?>"
                    >
                        <div class="single-contest-card__title">
                            <h2><?php the_title(); ?></h2>
                        </div>
                        <div class="single-contest-card__image"
                             style="background-image: url('<?php echo(!empty($bg_image) ? $bg_image : ''); ?>')"
                        >
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
        </div>
    <?php endif;

}

add_shortcode('past_contests_shortcode', 'custom_yz_past_contests_shortcode');

function exclude_tab_if_not_personal_profile() {
    if ( ! bp_is_my_profile() ) {
        bp_core_remove_nav_item( 'my-contests' );
    }
}
add_action( 'bp_init', 'exclude_tab_if_not_personal_profile' );

