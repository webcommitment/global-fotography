<div class="container">
    <div class="row">
        <div class="col">
            <section class="contest-status-lower-header">
                <div class="contest-status-lower-header__back">
                    <a href="<?php echo get_the_permalink($contest); ?>">Cancel voting</a>
                </div>
                <div class="contest-status-lower-header__title">
                    <h1>
                        <?php echo __('Vote', 'olympus-child'); ?>
                    </h1>
                </div>
                <div class="contest-status-lower-header__submit">
                    <button id="submit-votes" class="primary-button submit-votes-button">
                        <span>
                            <?php echo __('Submit votes','olympus-child') ?>
                        </span>
                    </button>
                </div>
            </section>
        </div>
    </div>
</div>
