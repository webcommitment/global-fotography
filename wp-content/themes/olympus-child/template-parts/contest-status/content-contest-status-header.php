<section class="contest-status-header">
    <div class="container">
        <?php get_template_part('template-parts/common/content', 'user-score'); ?>
        <div class="row">
            <div class="col-12">
                <nav class="contest-status-nav">
                    <ul class="contest-status-nav__list">
                        <?php
                        $terms = get_terms('contest_status', array(
                            'hide_empty' => false,
                        ));
                        ?>
                        <?php foreach ($terms as $term): ?>
                            <li class="contest-status-nav__list-item <?php echo (get_queried_object()->term_id == $term->term_id) ? 'active-term' : ''; ?>">
                                <a href="<?php echo get_term_link($term); ?>">
                                    <?php echo $term->name; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col">
            <section class="contest-status-lower-header">
                <div class="contest-status-lower-header__back">
                    <a href="<?php echo get_home_url() . '/members/' . get_current_user(); ?>"><?php echo __('Back to profile','olympus-child') ?></a>
                </div>
                <div class="contest-status-lower-header__boosters">
                    <?php get_template_part('template-parts/common/content', 'boosters'); ?>
                </div>
            </section>
        </div>
    </div>
</div>
