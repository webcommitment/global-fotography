<?php
$total_votes = GFCore_Votes::get_users_total_votes(get_current_user_id());
?>
<div class="user-score">
    <div class="user-score__title">
        <?php echo __('Your score', 'olympus-child'); ?>
    </div>
    <div class="user-score__count">
        <?php
        echo $total_votes ?>
    </div>
</div>