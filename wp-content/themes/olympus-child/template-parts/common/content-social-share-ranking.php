<a class="social-share-button" href="#" data-toggle="modal" data-target="#shareModal">
    <?php echo __('Share my entry', 'wc-theme'); ?>
</a>

<?php
$url = get_permalink();
$title = __('My submission in ', 'olympus-child') . get_the_title() . __(' contest.', 'olympus-child');
?>
<div class="modal" tabindex="-1" role="dialog" id="shareModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo __('Share on social media', 'olympus-child'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p><?php echo __('Share your contest entry on one of the following social media or copy the link', 'olympus-child'); ?></p>
                <div class="social-btns">
                    <a class="social-button whatsapp-btn"
                       href="whatsapp://send?text=<?php echo __('See my entry in ', 'wc-theme') . get_the_title() . ' ' . get_permalink(); ?>"
                       data-action="share/whatsapp/share"><span class="fab fa-whatsapp"></span>Whatsapp</a>
                    <a class="social-button fb-btn"
                       href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>"
                       onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                       target="_blank" title="Share on Facebook"><span class="fab fa-facebook"></span>Facebook</a>
                    <a class="social-button twitter-btn"
                       href="https://twitter.com/share?url=<?php echo $url; ?>&text=<?php echo $title; ?>"
                       onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                       target="_blank" title="Share on Twitter"><span class="fab fa-twitter"></span>Twitter</a>
                    <button href="#" id="copyButton" class="social-button clipboard-btn"><span class="fa fa-copy"></span>Copy link</button>
                    <input type="text" id="copyTarget" class="social-button clipboard-btn" value="<?php echo get_permalink(); ?>">
                </div>
                <input type="hidden" value="<?php echo get_permalink(); ?>" id="siteUrl">
            </div>
        </div>
    </div>
</div>