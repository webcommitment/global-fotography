<?php
$boosters = GF_Core_Public::get_boosters();
?>

<?php foreach ($boosters as $booster): ?>
<?php
    $product_id = get_field($booster['slug'] . '_related_product', 'option');
    $pts_balance = GF_Core_Public::get_current_users_points_balance_by_type(get_current_user_id(), $booster['slug']);
?>

<div class="modal booster-modal fade" id="<?php echo $booster['slug']; ?>_modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo $booster['slug']; ?>_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="<?php echo $booster['slug']; ?>_modal"><?php
                    echo get_field($booster['slug'] . '_modal_title','option');
                    ?></h5>
                        <?php if(!empty($pts_balance)): ?>
                            <span class="badge badge-success">
                                <strong id="pts_balance_badge"
                                    data-balance="<?php echo $pts_balance; ?>"
                                >
                                    <?php echo $pts_balance; ?>
                                </strong>
                                <?php
                                echo $booster['title'] . __(' point', 'olympus-child');
                                echo ($pts_balance > 1) ? 's' : '';
                                echo __(' left', 'olympus-child');
                                ?>
                            </span>
                        <?php endif; ?>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                    echo get_field($booster['slug'] . '_modal_content','option');
                ?>
            </div>
            <div class="modal-footer">
                <?php
                if (GF_Core_Public::woo_is_in_cart($product_id)) {
                    $link = wc_get_cart_url();
                } else {
                    $link = $current_url . '?add-to-cart=' . $product_id;
                }
                ?>
                <a href="<?php echo $link; ?>" id="get-booster" type="button" class="btn btn-primary">
                    <span>
                        <?php echo __('Buy a ','olympus-child') . $booster['title']; ?>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>