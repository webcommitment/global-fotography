<?php
$boosters = GF_Core_Public::get_boosters();
$trade_balance = GF_Core_Public::get_current_users_points_balance_by_type(get_current_user_id(), 'trade_pts');
$charge_balance = GF_Core_Public::get_current_users_points_balance_by_type(get_current_user_id(), 'charge_pts');
$boost_balance = GF_Core_Public::get_current_users_points_balance_by_type(get_current_user_id(), 'boost_pts');
$dir = get_template_directory_uri() . '-child';
?>

<nav class="contest-boosters">
    <ul class="contest-boosters__list">
        <?php foreach ($boosters as $booster): ?>
            <li class="contest-boosters__list-item">
                <a href="#" class="primary-button"
                   data-toggle="modal"
                   data-target="#<?php echo $booster['slug']; ?>_modal"
                >
                <span class="icon">
                    <?php
                    if ($booster['slug'] === 'trade_pts') :
                        echo file_get_contents($dir . "/img/icons/switch.svg");
                    elseif ($booster['slug'] === 'charge_pts'):
                        echo file_get_contents($dir . "/img/icons/power-plug.svg");
                    elseif ($booster['slug'] === 'boost_pts'):
                        echo file_get_contents($dir . "/img/icons/startup.svg");
                    endif;
                    ?>
                </span>
                    <span>
                    <?php echo $booster['title']; ?>
                </span>
                </a>
                <?php if ($booster['slug'] === 'trade_pts' && $trade_balance > 0) : ?>
                    <span class="contest-boosters__count">
                        <?php echo $trade_balance; ?>
                    </span>
                <?php elseif ($booster['slug'] === 'charge_pts' && $charge_balance > 0): ?>
                    <span class="contest-boosters__count">
                        <?php echo $charge_balance; ?>
                    </span>
                <?php elseif ($booster['slug'] === 'boost_pts' && $boost_balance > 0): ?>
                    <span class="contest-boosters__count">
                        <?php echo $boost_balance; ?>
                    </span>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
</nav>