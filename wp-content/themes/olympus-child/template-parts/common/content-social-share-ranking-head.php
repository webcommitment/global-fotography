<?php
$image_url = get_field('winner_gallery','option')[0]['sizes']['large'];

print_r($image_url);
$entry_id = GFCore_Contest_Entries::get_users_contest_entry_id(get_the_ID());
$contest_status = wp_get_post_terms(get_the_ID(), 'contest_status')[0]->slug;
$vote_count = GFCore_Contest_Entries::get_users_content_entry_data($entry_id)['total_votes'];
if ($contest_status !== 'closed-contests'):
    $description = __('See my submission on Global Fotography contest.', 'wc-theme');
else:
    $description = __('See my result on Global Fotography contest.', 'wc-theme');
endif;
?>
<meta property="og:url"           content="<?php echo get_permalink(); ?>" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="<?php echo __('My submission on Global Fotography contest.', 'wc-theme');  ?>" />
<meta property="og:description"   content="<?php echo $description; ?>" />
<meta property="og:image"         content="<?php echo $image_url; ?>" />
