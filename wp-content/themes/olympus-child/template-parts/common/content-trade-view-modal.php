<?php
global $post;
$post_slug = $post->post_name;
$entry_id = GFCore_Contest_Entries::get_users_contest_entry_id($post->ID);

$has_enough_trade_points = false;
$has_enough_charge_points = false;
$has_enough_boost_points = false;
$trade_balance = GF_Core_Public::get_current_users_points_balance_by_type(get_current_user_id(), 'trade_pts');
$charge_balance = GF_Core_Public::get_current_users_points_balance_by_type(get_current_user_id(), 'charge_pts');
$boost_balance = GF_Core_Public::get_current_users_points_balance_by_type(get_current_user_id(), 'boost_pts');

if ($trade_balance > 0) {
    $has_enough_trade_points = true;
}
if ($charge_balance > 0) {
    $has_enough_charge_points = true;
}
if ($boost_balance > 0) {
    $has_enough_boost_points = true;
}
$trade_product_id = get_field('trade_related_product', 'option');
$boost_product_id = get_field('boost_related_product', 'option');
$charge_product_id = get_field('charge_related_product', 'option');

$dir = get_template_directory_uri() . '-child';
?>

<div class="modal trade_view_modal fade" id="trade_view_modal" tabindex="-1" role="dialog"
     aria-labelledby="trade_view_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="trade_view_modal_modal">
                    <?php
                    echo __('Manage your Entry', 'olympus-child');
                    ?>
                    <span class="badge badge-success">
                            <strong id="trade_balance_badge"
                                    data-balance="<?php echo $trade_balance; ?>"
                            >
                                <?php echo $trade_balance; ?>
                            </strong>
                            <?php
                            echo __('Trade point', 'olympus-child');
                            echo ($trade_balance > 1) ? 's' : '';
                            echo __(' left', 'olympus-child');
                            ?>
                        </span>
                    <span class="badge badge-success">
                                    <strong id="charge_balance_badge"
                                            data-balance="<?php echo $charge_balance; ?>"
                                    >
                                        <?php echo $charge_balance; ?>
                                    </strong>
                                    <?php
                                    echo __('Charge point', 'olympus-child');
                                    echo ($charge_balance > 1) ? 's' : '';
                                    echo __(' left', 'olympus-child');
                                    ?>
                        </span>
                    <span class="badge badge-success">
                                <strong id="boost_balance_badge"
                                        data-balance="<?php echo $boost_balance; ?>"
                                >
                                    <?php echo $boost_balance; ?>
                                </strong>
                                <?php
                                echo __('Boost point', 'olympus-child');
                                echo ($boost_balance > 1) ? 's' : '';
                                echo __(' left', 'olympus-child');
                                ?>
                            </span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body <?php echo (!$has_enough_boost_points ? 'not-enough-boost' : ''); ?>">
                <?php
                acf_form(array(
                    'post_id' => $entry_id,
                    'new_post' => false,
                    'post_title' => false,
                    'post_content' => false,
                    'submit_value' => __('Submit photos'),
                    'html_after_fields' => '
                    <input type="hidden" name="usr_bal" value="' . $trade_balance . '"/>
                    <input type="hidden" name="used_charge_pts" value="0"/>
                    <input type="hidden" name="used_boost_pts" value="0"/>
                    <input type="hidden" name="used_trade_pts" value="0"/>
                    <input type="hidden" name="related_contest" value="'. $post_slug .'"/>
                    '
                ));
                ?>
            </div>
            <div class="modal-footer">

                <?php if ($has_enough_trade_points): ?>

                    <a href="#" id="trade_btn" type="button" class="btn btn-primary disabled">
                        <span class="icon">
                            <?php echo file_get_contents($dir . "/img/icons/switch.svg"); ?>
                        </span>
                        <span>
                            <?php echo __('Trade photo(s)', 'olympus-child'); ?>
                        </span>
                    </a>

                <?php else: ?>
                    <?php echo __('Want to change your photo(s) without losing votes?', 'olympus-child'); ?>
                    <a href="<?php echo get_site_url() . '/?add-to-cart=' . $trade_product_id; ?>" id="buy-trade-pts"
                       type="button"
                       class="btn btn-primary">
                    <span>
                        <?php echo __('Get Trade points', 'olympus-child'); ?>
                    </span>
                    </a>
                <?php endif; ?>

                <?php if ($has_enough_charge_points): ?>
                    <a href="#" id="charge_btn" type="button" class="btn btn-primary">
                            <span class="icon">
                                <?php echo file_get_contents($dir . "/img/icons/power-plug.svg"); ?>
                            </span>
                            <span>
                                <?php echo __('Charge photos', 'olympus-child'); ?>
                            </span>
                    </a>
                <?php else: ?>
                    <a href="<?php echo get_site_url() . '/?add-to-cart=' . $charge_product_id; ?>" id="buy-charge-pts"
                       type="button"
                       class="btn btn-primary">
                    <span>
                        <?php echo __('Get Charge points', 'olympus-child'); ?>
                    </span>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>