<div class="container">
    <div class="row">
        <div class="col">
            <section class="contest-status-lower-header">
                <?php if (get_current_user()): ?>
                    <div class="contest-status-lower-header__back">
                        <a href="<?php echo get_home_url() . '/members/' . get_userdata(get_current_user_id())->user_login; ?>"><?php echo __('Back to profile','olympus-child') ?></a>
                    </div>
                <?php endif; ?>

                <div class="contest-status-lower-header__title">
                    <h1>
                        <?php echo get_the_title(); ?>
                    </h1>
                </div>
                <div class="contest-status-lower-header__freespace">

                </div>
            </section>
        </div>
    </div>
</div>
