<?php

$result = GFCore_Contest_Entries::get_users_content_entry_data(get_the_ID());
$user = get_user_by('ID', $result['author']);
$charge_balance = GF_Core_Public::get_current_users_points_balance_by_type($result['author'], 'charge_pts');
$boost_balance = GF_Core_Public::get_current_users_points_balance_by_type($result['author'], 'boost_pts');

$has_enough_charge_points = false;
$has_enough_boost_points = false;

if ($charge_balance > 0) {
    $has_enough_charge_points = true;
}
if ($boost_balance > 0) {
    $has_enough_boost_points = true;
}

$is_current_user = true;

$entry_id = $result['entry_id'];
$contest_id = $result['contest_id'];
$contest_title = get_the_title($contest_id);
$contest_link = get_home_url() . '/contest-vote/?contest=' . $contest_id;

?>
<div class="results-list__item">
    <div class="row">
        <div class="results-list__contest-info">
            <h3 class="results-list__contest-title">
                <?php echo $contest_title; ?>
            </h3>
            <div class="result-list__contest-time">
                <div class="results-list__meta">
                        <div class="results-list__meta--time">
                                                 <span>
                                                    <?php echo __('Time left', 'olympus-child'); ?>
                                                </span>
                            <strong class="clock_counter" id="clockdiv"
                                    data-deadline="<?php echo get_field('contest_end_time_and_date', $contest_id); ?>">
                                <div>
                                    <span class="days"></span>
                                    <div class="smalltext">d</div>
                                </div>
                                <div>
                                    <span class="hours"></span>
                                    <div class="smalltext">h</div>
                                </div>
                                <div>
                                    <span class="minutes"></span>
                                    <div class="smalltext">min</div>
                                </div>
                                <div>
                                    <span class="seconds"></span>
                                    <div class="smalltext">s</div>
                                </div>
                            </strong>
                        </div>
                </div>
            </div>
            <div class="results-list__contest-action">
                <a href="<?php echo $contest_link; ?>">
                    <?php echo __('Vote', 'olympus-child'); ?>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3">
            <div class="results-list__user">
                <div class="results-list__user__photo">
                    <img src="<?php echo get_avatar_url($result['author']); ?>"
                         alt="avatar-<?php echo $user->user_login; ?>"
                    >
                </div>

                <div class="results-list__user__position">
                    <span><?php echo $user->user_login; ?></span>
                </div>

                <div class="results-list__user__vote-count">
                                <span>
                                     <?php
                                     echo __('Total votes: ', 'olympus-child');
                                     ?>
                                </span>
                    <strong>
                        <?php echo $result['total_votes']; ?>
                    </strong>
                </div>

                <?php if ($is_current_user): ?>
                    <?php
                    $exposures = array();
                    for ($i = 1; $i <= 4; $i++) :
                        $exposures[$i] = (int) get_post_meta($entry_id, 'contest_image_'. $i . '_group_photo_priority_' . $i, true);
                    endfor;
                    $total_exposure_perc = round(array_sum($exposures) / 4 / 1440 * 100); //5760 = 24h (in minutes) * 4
                    ?>
                    <div class="results-list__user__total-exposure">
                        <div class="exposure-bar">
                            <div style="width:<?php echo $total_exposure_perc; ?>%;"
                                 class="exposure-bar__level">
                            </div>
                            <span><?php echo __('Exposure', 'olympus-child'); ?></span>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($has_enough_charge_points && $is_current_user):

                    ?>
                    <div class="results-list__user__charge">
                        <form method="post" action="<?php echo esc_url(get_permalink()); ?>">
                            <?php wp_nonce_field('charge_exposure_levels', 'charge_exposure_levels_nonce'); ?>
                            <button id="charge" type="submit" class="btn btn-primary">
                                            <span class="icon">
                                                <?php echo file_get_contents($dir . "/img/icons/power-plug.svg"); ?>
                                            </span>
                                <span>
                                                <?php echo __('Charge', 'olympus-child'); ?>
                                            </span>
                                <input type="hidden" name="contest_id"
                                       value="<?php echo $contest_id; ?>">
                                <input type="hidden" name="contest_entry_id"
                                       value="<?php echo $entry_id; ?>">
                            </button>
                        </form>
                    </div>
                <?php endif; ?>

            </div>
        </div>
        <div class="col-9">
            <div class="results-list__images">
                <div class="results-list__images__list">
                    <?php
                    $images = $result['images'];
                    $i = 1;
                    foreach ($images as $image):
                        if($image['image']['sizes']['medium']):
                        ?>
                        <div class="results-list__images__item">

                            <div class="results-list__images__item__image"
                                 style=" background-image: url('<?php echo $image['image']['sizes']['medium'] ?>');">
                            </div>

                            <?php if ($is_current_user):
                                $exposure_level = (int) get_post_meta($entry_id, 'contest_image_'. $i . '_group_photo_priority_' . $i, true);
                                $exposure_perc = round($exposure_level / 1440 * 100);
                                ?>
                                <div class="results-list__images__item__photo-exposure"
                                     data-exposure="<?php echo $exposure_perc; ?>"
                                >
                                    <div class="exposure-bar">
                                        <div style="width:<?php echo $exposure_perc; ?>%;"
                                             class="exposure-bar__level">

                                        </div>
                                        <span><?php echo __('Exposure', 'olympus-child'); ?></span>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="results-list__images__item__results">
                                <div class="results-list__images__item__results--left">
                                                <span>
                                                    <?php echo __('Votes', 'olympus-child'); ?>
                                                </span>
                                    <strong>
                                        <?php echo $image['vote_count']; ?>
                                    </strong>
                                </div>

                                <?php if ($has_enough_boost_points && $is_current_user): ?>
                                    <div class="results-list__images__item__results--right">
                                        <div class="results-list__images__item__boost">
                                            <form method="post"
                                                  action="<?php echo esc_url(get_permalink()); ?>">
                                                <?php wp_nonce_field('boost_exposure_level', 'boost_exposure_level_nonce'); ?>
                                                <button href="#"
                                                        id="boost_btn"
                                                        type="submit"
                                                        type="button"
                                                        class="btn btn-primary"
                                                >
                                                                <span class="icon">
                                                                    <?php echo file_get_contents($dir . "/img/icons/startup.svg"); ?>
                                                                </span>
                                                    <span>
                                                                    <?php echo __('Boost', 'olympus-child'); ?>
                                                                </span>
                                                </button>
                                                <input type="hidden" name="contest_id"
                                                       value="<?php echo $contest_id; ?>">
                                                <input type="hidden" name="contest_entry_id"
                                                       value="<?php echo $entry_id; ?>">
                                                <input type="hidden" name="photo_id"
                                                       value="<?php echo $i; ?>">
                                            </form>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php
                        $i++;
                        endif;
                    endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>