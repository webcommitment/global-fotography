<?php
$results = GFCore_Votes::get_contest_results(get_the_ID());

$charge_balance = GF_Core_Public::get_current_users_points_balance_by_type(get_current_user_id(), 'charge_pts');
$boost_balance = GF_Core_Public::get_current_users_points_balance_by_type(get_current_user_id(), 'boost_pts');

$has_enough_charge_points = false;
$has_enough_boost_points = false;

if ($charge_balance > 0) {
    $has_enough_charge_points = true;
}
if ($boost_balance > 0) {
    $has_enough_boost_points = true;
}

$dir = get_template_directory_uri() . '-child';
?>
<?php if (!empty($results)): ?>
    <div class="results-list">
        <?php foreach ($results as $index => $result): ?>
            <?php
            $user = get_userdata($result['author']);
            $is_current_user = false;

            if ($user == wp_get_current_user()) {
                $is_current_user = true;
            }
            $place = $index + 1;
            $entry_id = $result['entry_id'];

            ?>
            <div class="results-list__item">

                <div class="row">
                    <div class="col-3">
                        <div class="results-list__user">
                            <div class="results-list__user__photo">
                                <img src="<?php echo get_avatar_url($result['author']); ?>"
                                     alt="avatar-<?php echo $user->user_login; ?>"
                                >
                            </div>

                            <div class="results-list__user__position">
                                <span><?php echo $place . '.'; ?></span>
                                <span><?php echo $user->user_login; ?></span>
                            </div>

                            <div class="results-list__user__vote-count">
                                <span>
                                     <?php
                                     echo __('Total votes: ', 'olympus-child');
                                     ?>
                                </span>
                                <strong>
                                    <?php echo $result['total_votes']; ?>
                                </strong>
                            </div>

                            <?php if ($is_current_user): ?>
                                <?php
                                $exposures = array();
                                for ($i = 1; $i <= 4; $i++) :
                                    $value = (int) get_post_meta($entry_id, 'contest_image_'. $i . '_group_photo_priority_' . $i, true);
                                    $exposures[$i] = $value;
                                endfor;


                                $total_exposure_perc = round(array_sum($exposures) / 4 / 1440 * 100); //5760 = 24h (in minutes) * 4
                                ?>
                                <div class="results-list__user__total-exposure">
                                    <div class="exposure-bar">
                                        <div style="width:<?php echo $total_exposure_perc; ?>%;"
                                             class="exposure-bar__level">
                                        </div>
                                        <span><?php echo __('Exposure', 'olympus-child'); ?></span>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php if ($has_enough_charge_points && $is_current_user): ?>
                                <div class="results-list__user__charge">
                                    <form method="post"
                                          action="<?php echo esc_url(get_permalink()); ?>">
                                        <?php wp_nonce_field('charge_exposure_levels', 'charge_exposure_levels_nonce'); ?>
                                        <button href="#" id="charge_pts" type="submit" class="btn btn-primary">
                                            <span class="icon">
                                                <?php echo file_get_contents($dir . "/img/icons/power-plug.svg"); ?>
                                            </span>
                                            <span>
                                                <?php echo __('Charge', 'olympus-child'); ?>
                                            </span>
                                            <input type="hidden" name="contest_entry_id"
                                                   value="<?php echo $entry_id; ?>">
                                        </button>
                                    </form>
                                </div>
                                <?php get_template_part('template-parts/common/content', 'social-share-ranking'); ?>
                            <?php endif; ?>

                        </div>
                    </div>
                    <div class="col-9">
                        <div class="results-list__images">
                            <div class="results-list__images__list">
                                <?php
                                $images = $result['images'];
                                $i = 1;
                                foreach ($images as $image):
                                    if ($image['image']['sizes']['medium']) : ?>
                                    <div class="results-list__images__item">
                                        <a href="<?php echo $image['image']['url']; ?>"
                                                data-fancybox="gallery-<?php echo $entry_id; ?>"
                                                data-caption="by <?php echo $user->user_login; ?>"
                                             class="results-list__images__item__image"
                                             style="background-image: url('<?php echo $image['image']['sizes']['medium'] ?>');">
                                        </a>
                                        <?php if ($is_current_user):
                                            $exposure_level = (int) get_post_meta($entry_id, 'contest_image_'. $i . '_group_photo_priority_' . $i, true);
                                            $exposure_perc = round($exposure_level / 1440 * 100);
                                            ?>
                                            <div class="results-list__images__item__photo-exposure"
                                                 data-exposure="<?php echo $exposure_perc; ?>"
                                            >
                                                <div class="exposure-bar">
                                                    <div style="width:<?php echo $exposure_perc; ?>%;"
                                                         class="exposure-bar__level">
                                                    </div>
                                                    <span><?php echo __('Exposure', 'olympus-child'); ?></span>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <div class="results-list__images__item__results">
                                            <div class="results-list__images__item__results--left">
                                                <span>
                                                    <?php echo __('Votes', 'olympus-child'); ?>
                                                </span>
                                                <strong>
                                                    <?php echo $image['vote_count']; ?>
                                                </strong>
                                            </div>

                                            <?php if ($has_enough_boost_points && $is_current_user): ?>
                                                <div class="results-list__images__item__results--right">
                                                    <div class="results-list__images__item__boost">
                                                        <form method="post"
                                                              action="<?php echo esc_url(get_permalink()); ?>">
                                                            <?php wp_nonce_field('boost_exposure_level', 'boost_exposure_level_nonce'); ?>
                                                            <button href="#"
                                                                    id="boost_btn"
                                                                    type="submit"
                                                                    type="button"
                                                                    class="btn btn-primary"
                                                            >
                                                                <span class="icon">
                                                                    <?php echo file_get_contents($dir . "/img/icons/startup.svg"); ?>
                                                                </span>
                                                                <span>
                                                                    <?php echo __('Boost', 'olympus-child'); ?>
                                                                </span>
                                                            </button>
                                                            <input type="hidden" name="contest_entry_id"
                                                                   value="<?php echo $entry_id; ?>">
                                                            <input type="hidden" name="photo_id"
                                                                   value="<?php echo $i; ?>">
                                                        </form>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php
                                    $i++;
                                    endif;
                                endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>