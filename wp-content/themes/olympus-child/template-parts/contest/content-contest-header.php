<?php
global $post;
$post_slug = $post->post_name;
$already_voted = GFCore_Votes::already_voted(get_the_ID());
$is_active_contest = GFCore_Contests::is_active_contest($post);
$already_submitted = GFCore_Contest_Entries::already_submitted(get_the_ID());

$entries_count = GFCore_Contest_Entries::get_contest_entry_items_count(get_the_ID());
$prizes_excerpt = get_field('prizes_excerpt');
$contest_type = wp_get_post_terms(get_the_ID(), 'contest_type')[0]->slug;
$unlocked_contests = get_field('unlocked_contests', 'user_' . get_current_user_id());
$unlocked = false;
$contest_product = get_field('select_contest_product', get_the_ID());
if (! empty($contest_product)) {
    $product = wc_get_product($contest_product[0]->ID);
    $price = $product->get_price();
}
global $wp;
$current_url = home_url($wp->request);

if (is_array($unlocked_contests)) {
    foreach ($unlocked_contests as $unlocked_contest) {
        if ($unlocked_contest->ID == get_the_ID()) {
            $unlocked = true;
        }
    }
} else {
    if ($unlocked_contests->ID == get_the_ID()) {
        $unlocked = true;
    }
}

if ($contest_type === 'free') {
    $unlocked = true;
}

if ($unlocked === false) {
    if (GF_Core_Public::woo_is_in_cart($product->get_id())) {
        $link = wc_get_cart_url();
    } else {
        $link = $current_url . '?add-to-cart=' . $product->get_id();;
    }
}

?>

<section class="contest-header">
    <div class="container">
        <?php get_template_part('template-parts/common/content', 'content-user-score'); ?>
        <div class="row">
            <div class="col-md-3">
                <?php if ($is_active_contest): ?>
                    <?php if ($unlocked): ?>
                        <nav class="contest-actions">
                            <div class="contest-actions__vote">
                                <a class="primary-button <?php echo ($already_voted != 1 || $entries_count == 0) ? '' : 'button-disabled'; ?>"
                                   href="<?php echo ($already_voted != 1 || $entries_count == 0) ? add_query_arg('contest', get_the_ID(), get_home_url() . '/contest-vote') : '#'; ?>"
                                >
                                    <span><?php echo __('Vote', 'olympus-child'); ?></span>
                                </a>
                            </div>
                            <div class="contest-actions__submit-photos">
                                <?php if ($already_submitted != 1): ?>
                                    <button type="button"
                                            class="primary-button"
                                            data-toggle="modal"
                                            data-target="#register_modal"
                                    >
                                        <span><?php echo __('Submit photos', 'olympus-child'); ?></span>
                                    </button>
                                <?php else: ?>
                                    <a class="primary-button"
                                       data-toggle="modal"
                                       data-target="#trade_view_modal"
                                    >
                                        <span><?php echo __('My entry', 'olympus-child'); ?></span>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </nav>
                    <?php else: ?>
                        <nav class="contest-actions">
                            <a class="primary-button"
                               href="<?php echo $link; ?>">
                                <span><?php echo __('Join for ', 'olympus-child') . get_woocommerce_currency_symbol() . $price ?></span>
                            </a>
                        </nav>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="col-md-9">
                <div class="contest-meta">
                    <div class="contest-meta__wrapper">
                        <?php
                        if (!empty($prizes_excerpt)):
                            ?>
                            <div class="contest-meta__prizes">
                                <span>
                                    <?php echo __('Prizes', 'olympus-child'); ?>
                                </span>
                                <strong>
                                    <?php echo $prizes_excerpt; ?>
                                </strong>
                            </div>
                        <?php endif; ?>
                        <?php if ($is_active_contest): ?>
                            <div class="contest-meta__time-left">
                                 <span>
                                    <?php echo __('Time left', 'olympus-child'); ?>
                                </span>
                                <strong class="clock_counter"
                                        data-deadline="<?php echo get_field('contest_end_time_and_date'); ?>">
                                    <div>
                                        <span class="days"></span>
                                        <div class="smalltext"><?php echo __('d', 'olympus-child'); ?></div>
                                    </div>
                                    <div>
                                        <span class="hours"></span>
                                        <div class="smalltext"><?php echo __('h', 'olympus-child'); ?></div>
                                    </div>
                                    <div>
                                        <span class="minutes"></span>
                                        <div class="smalltext"><?php echo __('m', 'olympus-child'); ?></div>
                                    </div>
                                    <div>
                                        <span class="seconds"></span>
                                        <div class="smalltext"><?php echo __('s', 'olympus-child'); ?></div>
                                    </div>
                                </strong>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($entries_count)): ?>
                            <div class="contest-meta__entries">
                            <span>
                                <?php echo __('Entries', 'olympus-child'); ?>
                            </span>
                                <strong>
                                    <?php
                                    echo $entries_count;
                                    ?>
                                </strong>
                            </div>
                        <?php endif; ?>
                    </div>
                    <h1 class="contest-meta__title">
                        <?php the_title(); ?>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</section>