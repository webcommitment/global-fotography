<!-- Modal -->
<div class="modal fade" id="register_modal" tabindex="-1" role="dialog" aria-labelledby="register_modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="register_modal">Submit photo's</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body not-enough-boost">
                <?php
                acf_form(array(
                    'post_id'		=> 'new_post',
                    'post_title'	=> false,
                    'post_content'	=> false,
                    'new_post'		=> array(
                        'post_type'		=> 'contest_entries',
                        'post_status'	=> 'publish'
                    ),
                    'submit_value'  => __('Submit photos'),
                    'html_after_fields' => '<input type="hidden" name="related_contest" value="'. $post->post_name .'"/>'
                ));
                ?>
            </div>
            <div class="modal-footer">
                <button id="submit-photos" type="button" class="btn btn-primary"><?php echo __('Submit photos','olympus-child'); ?></button>
            </div>
        </div>
    </div>
</div>