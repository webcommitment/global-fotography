<nav class="contest-page-nav">
    <?php
    $details_content = get_field('details_content');
    $prizes_content = get_field('prizes_content');
    $rules_content = get_field('rules_content');
    $dir = get_template_directory_uri() . '-child';
    ?>
    <div class="contest-page-nav__wrapper">
        <ul class="nav nav-tabs" id="product-summary-tabs" role="tablist">
            <?php if ($details_content) : ?>
                <li class="nav-item">
                    <a class="nav-link active" id="details-tab" data-toggle="tab"
                       href="#details_item" role="tab" aria-controls="home" aria-selected="true">
                        <span class="icon">
                             <?php echo file_get_contents($dir . "/img/icons/planet-earth.svg"); ?>
                        </span>
                        <span>
                             <?php echo __('Details', 'olympus-child'); ?>
                        </span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if ($prizes_content) : ?>
                <li class="nav-item">
                    <a class="nav-link" id="prizes-tab" data-toggle="tab" href="#prizes_item"
                       role="tab" aria-controls="prizes" aria-selected="false">
                        <span class="icon">
                            <?php echo file_get_contents($dir . "/img/icons/trophy.svg"); ?>
                        </span>
                        <span>
                            <?php echo __('Prizes', 'olympus-child'); ?>
                        </span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if ($rules_content) : ?>
                <li class="nav-item">
                    <a class="nav-link" id="rules-tab" data-toggle="tab" href="#rules_item"
                       role="tab" aria-controls="rules" aria-selected="false">
                        <span class="icon">
                            <?php echo file_get_contents($dir . "/img/icons/documents.svg"); ?>
                        </span>
                        <span>
                            <?php echo __('Rules', 'olympus-child'); ?>
                        </span>
                    </a>
                </li>
            <?php endif; ?>
            <li class="nav-item">
                <a class="nav-link" id="ranking-tab" data-toggle="tab" href="#ranking_item"
                   role="tab" aria-controls="ranking" aria-selected="false">
                    <span class="icon">
                        <?php echo file_get_contents($dir . "/img/icons/podium.svg"); ?>
                    </span>
                    <span>
                        <?php echo __('Ranking', 'olympus-child'); ?>
                    </span>
                </a>
            </li>
        </ul>
        <?php get_template_part('template-parts/common/content', 'boosters'); ?>
    </div>
    <div class="tab-content" id="product-summary-tabs-content">
        <?php if ($details_content) : ?>
            <div class="tab-pane fade show active details" id="details_item" role="tabpanel"
                 aria-labelledby="details-tab">
                <div class="row">
                    <div class="col-md-4 details__author">
                        <img src="<?php echo get_avatar_url(get_the_author()); ?>"
                             alt="avatar-<?php echo get_the_author(); ?>"
                        >
                        <div class="details__author--info">
                            <span>
                                <?php echo __('Created by','olympus-child'); ?>
                            </span>
                            <strong>
                                <?php echo get_the_author(); ?>
                            </strong>
                        </div>
                    </div>
                    <div class="col-md-8 details__content">
                        <div class="details__content--container">
                            <?php echo $details_content ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (have_rows('prizes_content')): ?>
            <div class="tab-pane fade" id="prizes_item" role="tabpanel"
                 aria-labelledby="prizes-tab">
                <ul>
                    <?php while (have_rows('prizes_content')): the_row();
                        $prize_image = get_sub_field('prize_image');
                        $content = get_sub_field('prize_content');
                        ?>
                        <li class="prizes">
                            <div class="row">
                                <div class="col-md-4 prizes__image">
                                    <img src="<?php echo $prize_image['sizes']['medium_large']; ?>"
                                    alt="<?php echo $prize_image['title']; ?>"
                                    />
                                </div>
                                <div class="col-md-8 prizes__content">
                                    <div class="prizes__content--container">
                                        <?php echo $content; ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        <?php endif; ?>

        <?php if (have_rows('rules_content')): ?>
            <div class="tab-pane fade" id="rules_item" role="tabpanel"
                 aria-labelledby="rules-tab">
                <ul>
                    <?php while (have_rows('rules_content')): the_row();
                        $rule_image = get_sub_field('rule_image');
                        $content = get_sub_field('rule_content');
                        ?>
                        <li class="rules">
                            <div class="row">
                                <div class="col-md-3 rules__image">
                                    <img src="<?php echo $rule_image['sizes']['medium_large']; ?>"
                                         alt="<?php echo $rule_image['title']; ?>"
                                    />
                                </div>
                                <div class="col-md-9 rules__content">
                                    <div class="rules__content--container">
                                    <?php echo $content; ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        <?php endif; ?>
        <div class="tab-pane fade" id="ranking_item" role="tabpanel"
             aria-labelledby="ranking-tab">
            <?php get_template_part('template-parts/contest/content', 'contest-results'); ?>
        </div>
    </div>
</nav>