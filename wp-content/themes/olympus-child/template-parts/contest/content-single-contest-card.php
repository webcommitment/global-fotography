<?php
$bg_image = GFCore_Contests::get_stunning_header_background_url(get_the_ID());
$entries_count = GFCore_Contest_Entries::get_contest_entry_items_count(get_the_ID());
$prizes_excerpt = get_field('prizes_excerpt');
$contest_status = wp_get_post_terms(get_the_ID(), 'contest_status')[0]->slug;
$contest_type = wp_get_post_terms(get_the_ID(), 'contest_type')[0]->slug;
$product = get_field('select_contest_product', get_the_ID())[0];

$link = get_permalink();

?>

<div class="single-contest-card col-md-6 col-lg-4">
    <a class="single-contest-card__inner"
        href="<?php echo $link; ?>"
    >
        <div class="single-contest-card__title">
            <h2><?php the_title(); ?></h2>
        </div>
        <div class="single-contest-card__image"
            style="background-image: url('<?php echo (!empty($bg_image)? $bg_image : ''); ?>')"
        >
        </div>
        <div class="single-contest-card__meta">
            <?php
            if (!empty($prizes_excerpt)):
            ?>
            <div class="single-contest-card__meta--prizes">
                <span>
                    <?php echo __('Prizes','olympus-child'); ?>
                </span>
                <strong>
                    <?php echo $prizes_excerpt; ?>
                </strong>
            </div>
            <?php endif; ?>
            <?php if ($contest_status === 'upcoming-contests'): ?>
                <div class="single-contest-card__meta--time">
                    <span>
                        <?php echo __('Starts: ','olympus-child'); ?>
                    </span>
                    <strong>
                        <?php echo get_field('contest_starting_time_and_date'); ?>
                    </strong>
                </div>
            <?php elseif ($contest_status !== 'closed-contests'): ?>
            <div class="single-contest-card__meta--time">
                 <span>
                    <?php echo __('Time left','olympus-child'); ?>
                </span>
                <strong class="clock_counter" id="clockdiv" data-deadline="<?php echo get_field('contest_end_time_and_date'); ?>">
                    <div>
                        <span class="days"></span>
                        <div class="smalltext">d</div>
                    </div>
                    <div>
                        <span class="hours"></span>
                        <div class="smalltext">h</div>
                    </div>
                    <div>
                        <span class="minutes"></span>
                        <div class="smalltext">min</div>
                    </div>
                    <div>
                        <span class="seconds"></span>
                        <div class="smalltext">s</div>
                    </div>
                </strong>
            </div>
            <?php endif; ?>

            <?php if (!empty($entries_count)): ?>
                <div class="single-contest-card__meta--entries">
                    <span>
                        <?php echo __('Entries','olympus-child'); ?>
                    </span>
                    <strong>
                        <?php
                             echo $entries_count;
                        ?>
                    </strong>
                </div>
            <?php endif; ?>
        </div>
    </a>
</div>