<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package olympus
 */

global $post;
$post_slug = $post->post_name;
$already_voted = GFCore_Votes::already_voted(get_the_ID());
$already_submitted = GFCore_Contest_Entries::already_submitted(get_the_ID());

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    $stunning_visibility = olympus_stunning_visibility();
    if ( $stunning_visibility !== 'yes' && ! olympus_is_composer() ) {
        the_title( '<h1 class="entry-title">', '</h1>' );
    } ?>

    <?php get_template_part( 'template-parts/contest/content', 'contest-tabs' ); ?>

    <?php if(!$already_submitted): ?>
        <?php get_template_part( 'template-parts/contest/content', 'contest-register-modal' ); ?>
    <?php endif; ?>

</article><!-- #apge-<?php the_ID(); ?> -->

<?php get_template_part('template-parts/common/content', 'booster-modal'); ?>

<?php if($already_submitted): ?>
    <?php get_template_part('template-parts/common/content', 'trade-view-modal'); ?>
<?php endif; ?>
