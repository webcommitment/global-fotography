<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    GF_Core
 * @subpackage GF_Core/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    GF_Core
 * @subpackage GF_Core/public
 * @author     Your Name <email@example.com>
 */
class GF_Core_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $gf_core The ID of this plugin.
     */
    private $gf_core;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @param string $gf_core The name of the plugin.
     * @param string $version The version of this plugin.
     * @since    1.0.0
     */
    public function __construct($gf_core, $version)
    {
        $this->gf_core = $gf_core;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in GF_Core_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The GF_Core_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->gf_core, plugin_dir_url(__FILE__) . 'css/gf-core-public.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in GF_Core_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The GF_Core_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->gf_core, plugin_dir_url(__FILE__) . 'js/gf-core-public.js', array('jquery'), $this->version, false);

    }

    public function add_custom_options_page()
    {
        if (function_exists('acf_add_options_sub_page')) {
            acf_add_options_sub_page(array(
                'page_title' => 'Contests configuration',
                'menu_title' => 'Contests configuration',
                'parent_slug' => 'edit.php?post_type=contests',
            ));
        }
    }

    public static function get_boosters()
    {
        return array(
            0 => array(
                'slug' => 'trade_pts',
                'title' => 'Trade'
            ),
            1 => array(
                'slug' => 'charge_pts',
                'title' => 'Charge'
            ),
            2 => array(
                'slug' => 'boost_pts',
                'title' => 'Boost'
            ),
        );
    }

    function update_unlocked_contests($order_id, $order)
    {
        if ($user_id = $order->get_customer_id()) {
            foreach ($order->get_items() as $item) {
                $contests = get_posts(array(
                    'post_type' => 'contests',
                    'meta_query' => array(
                        array(
                            'key' => 'select_contest_product',
                            'value' => '"' . $item->get_product_id() . '"',
                            'compare' => 'LIKE'
                        )
                    )
                ));

                if ( !empty($contests) && $order->get_user_id() > 0) {
                    $unlocked_contests = get_user_meta($order->get_user_id(), 'unlocked_contests')[0];
                    if (!empty($unlocked_contests)) {
                        $unlocked_contests[] = $contests[0]->ID;
                    } else {
                        $unlocked_contests = [$contests[0]->ID];
                    }
                    update_field('unlocked_contests', $unlocked_contests, 'user_' . $order->get_user_id());
                }
            }
        }
    }

    function update_user_point_balance($order_id, $order)
    {
        $trade_product_id = get_field('trade_related_product', 'option');
        $charge_product_id = get_field('charge_related_product', 'option');
        $boost_product_id = get_field('boost_related_product', 'option');

        if ($user_id = $order->get_customer_id()) {
            foreach ($order->get_items() as $item) {

                if ($trade_product_id == $item->get_product_id() && $order->get_user_id() > 0) {
                    $trade_pts = (int) get_user_meta($order->get_user_id(), 'trade_pts', true);
                    update_user_meta($order->get_user_id(), 'trade_pts', $trade_pts + $item->get_quantity());
                } elseif ($charge_product_id == $item->get_product_id() && $order->get_user_id() > 0) {
                    $charge_pts = (int) get_user_meta($order->get_user_id(), 'trade_pts', true);
                    update_user_meta($order->get_user_id(), 'charge_pts', $charge_pts + $item->get_quantity());
                } elseif ($boost_product_id == $item->get_product_id() && $order->get_user_id() > 0) {
                    $boost_pts = (int) get_user_meta($order->get_user_id(), 'trade_pts', true);
                    update_user_meta($order->get_user_id(), 'boost_pts', $boost_pts + $item->get_quantity());
                }
            }
        }
    }

    public static function get_current_users_points_balance_by_type($user_id, $type)
    {
        return get_user_meta($user_id, $type, false)[0];
    }

    function set_users_balances($user_id)
    {
        update_user_meta($user_id, 'vote_power', 1);
        update_user_meta($user_id, 'user_pts', 0);
        update_user_meta($user_id, 'trade_pts', 10);
        update_user_meta($user_id, 'charge_pts', 10);
        update_user_meta($user_id, 'boost_pts', 10);
    }

    function mycred_custom_references($references)
    {
        $references['winner'] = 'Won contest';
        $references['second'] = '2nd place';
        $references['third'] = '3rd place';
        $references['top10'] = 'Reached top 10';
        $references['top100'] = 'Reached top 100';
        $references['top10percent'] = 'Reached top 10%';
        $references['top20percent'] = 'Reached top 20%';
        $references['globalProPick'] = 'GlobalPro pick';

        return $references;
    }

    public static function woo_is_in_cart($product_id) {
        global $woocommerce;
        foreach($woocommerce->cart->get_cart() as $key => $val ) {
            $_product = $val['data'];
            if($product_id == $_product->get_id() ) {
                return true;
            }
        }
        return false;
    }

}
