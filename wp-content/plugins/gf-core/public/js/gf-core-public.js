(function ($) {
    'use strict';

    function getTimeRemaining(endtime) {
        const total = Date.parse(endtime) - Date.parse(new Date());
        const seconds = Math.floor((total / 1000) % 60);
        const minutes = Math.floor((total / 1000 / 60) % 60);
        const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
        const days = Math.floor(total / (1000 * 60 * 60 * 24));

        return {
            total,
            days,
            hours,
            minutes,
            seconds
        };
    }

    function initializeClock(item, endtime) {
        const clock = item;
        const daysSpan = clock.querySelector('.days');
        const hoursSpan = clock.querySelector('.hours');
        const minutesSpan = clock.querySelector('.minutes');
        const secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            const t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        const timeinterval = setInterval(updateClock, 1000);
    }

    function photoSrcs() {

        let photos = $('#trade_view_modal .acf-image-uploader img');
        let photoSrcs = [];

        photos.each(function (index) {
            photoSrcs[index] = photos[index].getAttribute('src');
        });

        return photoSrcs;
    }

    $(document).ready(function () {

        if ( document.getElementById("copyButton")) {

            document.getElementById("copyButton").addEventListener("click", function () {
                copyToClipboard(document.getElementById("copyTarget"));
                document.getElementById("copyButton").classList.add('copied');
                setTimeout(
                    function () {
                        document.getElementById("copyButton").classList.remove('copied');
                    }, 1000);
            });
        }

        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }

        $('#trade_view_modal .btn').on("click", function () {
            $(this).addClass('pending-btn');
        });

        $('#submit-photos') .on("click", function () {
            $(this).addClass('pending-btn');
        });

        $('.acf-field[data-name*="photo_priority"] .acf-input').on("click", function () {
            $(this).addClass('pending-btn');
        });


        $('[data-name="related_contest"] select, [data-name="photo_priority"] input, [data-name="vote_count_1"] input, [data-name="vote_count_2"] input, [data-name="vote_count_3"] input ').prop("disabled", true);

        if ($('.clock_counter').length) {
            $('.clock_counter').each(function (index) {
                const deadline = document.getElementsByClassName('clock_counter')[index].getAttribute('data-deadline');
                let item = document.getElementsByClassName('clock_counter')[index];
                initializeClock(item, deadline);
            });
        }

        if ($('#vote-page').length) {
            $('.masonry-item-inner').on("click", function () {
                var selected = $(this).hasClass('selected');
                var entryId = $(this).data('contest-entry-id');
                var contestEntryRow = $(this).data('contest-entry-row');
                var photoId = $(this).data('contest-photo-id');
                var image = $(this).data('image');

                if (selected !== true) {
                    $('.acf-actions a[data-event="add-row"]').click();
                    $('.acf-repeater .acf-row:nth-last-child(2)').attr('data-photo-id', photoId);
                    /*Set row data*/
                    $(".acf-repeater .acf-row[data-photo-id='" + photoId + "']").find('[data-name="contest_entry_id"] input').val(entryId);
                    $(".acf-repeater .acf-row[data-photo-id='" + photoId + "']").find('[data-name="contest_entry_row"] input').val(contestEntryRow);
                    $(".acf-repeater .acf-row[data-photo-id='" + photoId + "']").find('[data-name="contest_photo_id"] input').val(photoId);
                    $(".acf-repeater .acf-row[data-photo-id='" + photoId + "']").find('[data-name="image_url"] input').val(image);
                    $(".acf-repeater .acf-row[data-photo-id='" + photoId + "']").find('[data-name="vote_count"] input').val(1);
                    $(this).addClass('selected');

                    $.toast({
                        text: 'Photo <img src="' + image + '" style="width:33px; height:33px;"/> added to vote list',
                        showHideTransition: 'slide',
                        bgColor: '#16a085',
                        textColor: '#eee',
                        allowToastClose: true,
                        hideAfter: 2000,
                        stack: 5,
                        textAlign: 'left',
                        position: 'bottom-right'
                    })


                } else {
                    $(".acf-repeater .acf-row[data-photo-id='" + photoId + "']").find('a[data-event="remove-row"]').click();
                    $('a[data-event="confirm"]').click();
                    $(this).removeClass('selected');

                    $.toast({
                        text: 'Photo <img src="' + image + '" style="width:33px; height:33px;"/>  removed from vote list',
                        showHideTransition: 'slide',
                        bgColor: '#cf000f',
                        textColor: '#eee',
                        allowToastClose: true,
                        hideAfter: 2000,
                        stack: 5,
                        textAlign: 'left',
                        position: 'bottom-right'
                    })
                }

                $('.submit-votes-button').on("click", function () {
                    $('.acf-form-submit input').click();
                });

            });
        }

        if ($('#register_modal').length) {
            $('#submit-photos').on("click", function () {
                $('.acf-form-submit input').click();
            });
        }

        if ($('#trade_view_modal').length) {

            let initialPhotos = photoSrcs();
            
            $('.acf-image-uploader .-cancel').attr('title', 'Trade');

            $('#trade_view_modal .acf-image-uploader img').on('load', function () {
                let updatedImage = this.getAttribute('src');
                if (initialPhotos.indexOf(updatedImage) > -1) {
                } else {
                    $('#trade_btn').removeClass('disabled');
                    const tradeInput = $('input[name="used_trade_pts"]');
                    let tradeCount = Number(tradeInput.val()) + 1;
                    tradeInput.val(tradeCount);
                }
            });

            $('#trade_btn').on("click", function () {
                $('.acf-form-submit input').click();
            });

            var boost = $(".acf-field[data-name*='photo_priority']");
            $(boost).on("click", function () {
                const input = $('input', this);
                let num = Number(input.val()) + 1440;
                input.val(num);
                const boostInput = $('input[name="used_boost_pts"]');
                let boostCount = Number(boostInput.val()) + 1;
                boostInput.val(boostCount);
                $('.acf-form-submit input').click();
            });

            $('#charge_btn, #charge_btn_res').on("click", function () {
                const chargeInput = $('input[name="used_charge_pts"]');
                $(".acf-field[data-name*='photo_priority'] input").each(function(){
                    const input = $(this);
                    let num = Number(input.val()) + 1440;
                    input.val(num);
                });
                let chargeCount = Number(chargeInput.val()) + 1;
                chargeInput.val(chargeCount);
                $('.acf-form-submit input').click();
            });

        }
    });


})(jQuery);