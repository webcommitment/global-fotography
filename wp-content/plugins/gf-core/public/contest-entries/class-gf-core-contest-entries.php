<?php
/**
 * GF Core - Contests CPT
 */

require_once ABSPATH . '/wp-admin/includes/post.php';


class GFCore_Contest_Entries
{

    /**
     * Initialize the class and set its properties.
     *
     * @param string $plugin_name The name of this plugin.
     * @param string $version The version of this plugin.
     * @since    1.0.0
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register contest entries CPT
     */
    function contest_entries_cpt_init()
    {
        $labels = array(
            'name' => _x('Contest entry', 'post type general name', 'theme core'),
            'singular_name' => _x('Contest entry', 'post type singular name', 'theme core'),
            'menu_name' => _x('Contest entries', 'admin menu', 'theme core'),
            'name_admin_bar' => _x('Contest entries', 'add new on admin bar', 'theme core'),
            'add_new' => _x('Create new', 'theme core'),
            'add_new_item' => __('Add new contest entry', 'theme core'),
            'new_item' => __('New contest entry', 'theme core'),
            'edit_item' => __('Edit contest entry', 'theme core'),
            'view_item' => __('See contest entries', 'theme core'),
            'all_items' => __('Contest Entries', 'theme core'),
            'search_items' => __('Search contest entries:', 'theme core'),
            'not_found' => __('No contest entries found', 'theme core'),
            'not_found_in_trash' => __('No contest entries found in trash', 'theme core')
        );
        $args = array(
            'labels' => $labels,
            'description' => __('Description', 'theme core'),
            'menu_icon' => 'dashicons-images-alt',
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => 'edit.php?post_type=contests',
            'query_var' => true,
            'rewrite' => array('slug' => 'contest-entries'),
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => 6,
            'show_in_rest' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', 'author'),
            'taxonomies' => false
        );
        register_post_type('contest_entries', $args);
    }

    /**
     * Register contest taxonomy for contest entries CPT
     */
    function create_contest_taxonomy()
    {

        $labels = array(
            'name' => _x('Contest', 'taxonomy general name'),
            'singular_name' => _x('Contest', 'taxonomy singular name'),
            'search_items' => __('Search Contests'),
            'all_items' => __('All Contests'),
            'parent_item' => __('Parent Contests'),
            'parent_item_colon' => __('Parent Contests:'),
            'edit_item' => __('Edit Contests'),
            'update_item' => __('Update Contests'),
            'add_new_item' => __('Add New Contest'),
            'new_item_name' => __('New Contest Status'),
            'menu_name' => __('Contest'),
        );

        register_taxonomy('contest', array('contest_entries'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'vote'),
        ));
    }

    /**
     * Add related contest column
     * @param $columns
     * @return array
     */
    function add_acf_columns($columns)
    {
        return array_merge($columns, array(
            'related_contest' => __('Related Contest Item'),
        ));
    }

    /**
     * Add related contest column to contest entries list
     * @param $column
     * @param $post_id
     */
    function contest_entries_custom_column($column, $post_id)
    {
        switch ($column) {
            case 'related_contest':

                $post = get_field('related_contest', $post_id);

                echo
                    '<a href=' . get_edit_post_link($post) . ' target="_blank">' .
                    $post->post_title . '</a>';
                break;
        }
    }

    /**
     * Limit the media browser to users content
     * @param $where
     * @return string
     */
    function contest_entries_attachments_wpquery_where($where)
    {
        global $current_user;

        if (is_user_logged_in()) {
            if (isset($_POST['action'])) {
                if ($_POST['action'] == 'query-attachments') {
                    $where .= ' AND post_author=' . $current_user->data->ID;
                }
            }
        }
        return $where;
    }

    /**
     * Pre set the related contest field value
     * @param $field
     * @return mixed
     */
    function my_acf_prepare_field($field)
    {
        global $post;

        if (is_admin()) {
            return $field;
        }

        if (isset($post)) {
            $post_id = $post->ID;
            $field['value'] = $post_id;
        }
        return $field;

    }

    /**
     * Create new contest entry with related contest content
     * @param $post_id
     */
    function contest_entries_save_post($post_id)
    {
        $existing_post = $_POST['usr_bal'];
        $post_type = get_post_type($post_id);
        $related_contest = $_POST['related_contest'];
        $id = (int)$post_id;
        $related_contest_id = 0;
        $meta_key = 'related_contest';
        $current_user_id = get_current_user_id();
        $used_trade_points = $_POST['used_trade_pts'];
        $used_boost_points = $_POST['used_boost_pts'];
        $used_charge_points = $_POST['used_charge_pts'];

        if ($posts = get_posts(array(
            'name' => $related_contest,
            'post_type' => 'contests',
            'post_status' => 'publish',
            'posts_per_page' => 1
        )))
            $related_contest_id = $posts[0];

        if ($post_type !== 'contest_entries') {
            return $post_id;
        }

        if ($_POST['_acf_post_id'] !== 'new_post') {
            if (!is_admin()) {
                update_post_meta($id, $meta_key, $related_contest_id->ID);

                $trade_pts = (int)get_user_meta($current_user_id, 'trade_pts', true);
                update_user_meta($current_user_id, 'trade_pts', $trade_pts - $used_trade_points);

                $boost_pts = (int)get_user_meta($current_user_id, 'boost_pts', true);
                update_user_meta($current_user_id, 'boost_pts', $boost_pts - $used_boost_points);

                $charge_pts = (int)get_user_meta($current_user_id, 'charge_pts', true);
                update_user_meta($current_user_id, 'charge_pts', $charge_pts - $used_charge_points);

                return $post_id;
            }
        }

        // bail early if editing in admin
        if (is_admin()) {
            return;
        }

        if (!is_admin()) {
            if ($post_type === 'contest_entries' && empty($existing_post)) {

                $current_user = wp_get_current_user();

                // Set the post data
                $new_post = array(
                    'ID' => $post_id,
                    'post_title' => $related_contest . '-' . $current_user->user_login,
                );

                update_post_meta($id, $meta_key, $related_contest_id->ID);

                remove_action('acf/save_post', 'my_save_post', 20);
                // Update the post
                wp_update_post($new_post);
                // Add the hook back
                add_action('acf/save_post', 'my_save_post', 20);
            }
        } else {
            return $post_id;
        }

    }

    /**
     * Edit contest entry with related contest content
     * @param $post_id
     */
    function contest_entries_pre_save_post($post_id)
    {
        $id = (int)$post_id;
        $related_contest_id = 0;
        $meta_key = 'related_contest';
        $related_contest = $_POST['related_contest'];

        if ($posts = get_posts(array(
            'name' => $related_contest,
            'post_type' => 'contests',
            'post_status' => 'publish',
            'posts_per_page' => 1
        )))
            $related_contest_id = $posts[0];

        if ($_POST['_acf_post_id'] !== 'new_post') {
            update_post_meta($id, $meta_key, $related_contest_id->ID);
            return $post_id;
        }
    }

    /**
     * Get contest entry items (contest photos) from contest entries post type
     * @param $post
     * @return array
     */
    public static function get_contest_entry_items($post)
    {
        $contest_entries = new WP_Query(array(
            'post_type' => 'contest_entries',
            'author__not_in' => array(get_current_user_id()),
            'meta_query' => array(
                array(
                    'key' => 'related_contest',
                    'value' => array($post->ID),
                    'compare' => 'IN'
                )
            )
        ));

        $user_id = get_current_user_id();

        $user_votes = new WP_Query(array(
            'author'            =>  $user_id,
            'post_type'         =>  'votes',
            'post_status'       =>  'publish',
            'posts_per_page'    =>  -1,
            'meta_query'		=>  array(
                array(
                    'key' => 'contest_item',
                    'value' => array($post->ID),
                    'compare' => 'IN'
                )
            )
        ));

        $user_votes = $user_votes->posts;
        $index = 0;
        $user_vote_entries = array();

        foreach ($user_votes as $vote_entry) {
            while (have_rows('votes', $vote_entry->ID)): the_row();
                $contest_entry_id = get_sub_field('contest_entry_id' , $vote_entry->ID);
                $image_id = get_sub_field('contest_entry_row' , $vote_entry->ID);
                $user_vote_entries[$index] =
                    [
                        'vote_unique_id' => $contest_entry_id . '-' . $image_id,
                    ];
            $index++;
            endwhile;
        }

        $contest_items = array();
        $j = 1;
        for ($i = 1; $i <= 4; $i++) {
            foreach ($contest_entries->posts as $index => $entry) {
                while (have_rows('contest_image_' . $i . '_group', $entry->ID)): the_row();
                    $prio = get_sub_field('photo_priority_' . $i, $entry->ID);
                    $vote_count = get_sub_field('vote_count_' . $i, $entry->ID);

                    $contest_items[$j] =
                        [
                            'entry_id' => $entry->ID,
                            'image' => get_sub_field('contest_image_' . $i, $entry->ID)['url'],
                            'vote_unique_id' => $entry->ID . '-' . $i,
                            'image_row_key' => $i,
                            'vote_count' => $vote_count,
                            'exposure' => $vote_count + $prio,
                            'author' => $entry->post_author,
                        ];
                    $j++;
                endwhile;
            }
            $j++;
        }

        function removeElementWithValue($array, $key, $value){
            foreach($array as $subKey => $subArray){
                if($subArray[$key] == $value){
                    unset($array[$subKey]);
                }
            }
            return $array = array_values($array);
        }

        foreach ($user_vote_entries as $vote_entry) {
            $contest_items = removeElementWithValue($contest_items, "vote_unique_id", $vote_entry['vote_unique_id']);
        }

        usort($contest_items, function ($a, $b) {
            if ($a['exposure'] == $b['exposure'])
                return 0;
            else if ($a['exposure'] > $b['exposure'])
                return 1;
            else
                return -1;
        });

        return array_reverse($contest_items);
    }

    /**
     * Get contest entry items count (contest photos) from contest entries post type
     * @param $post
     * @return int
     */

    public static function get_contest_entry_items_count($id)
    {
        $result = new WP_Query(array(
            'post_type' => 'contest_entries',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => 'related_contest',
                    'value' =>  $id,
                    'compare' => '='
                ),
            )
        ));
        $posts = $result->posts;

        return (count($posts));
    }

    function prefix_admin_voting_form_callback()
    {
        status_header(200);
        die("Server received '{$_REQUEST['data']}' from your browser.");
        //request handlers should die() when they complete their task
    }

    public static function already_submitted($id)
    {
        $user_id = get_current_user_id();

        $result = new WP_Query(array(
            'author' => $user_id,
            'post_type' => 'contest_entries',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => 'related_contest',
                    'value' =>  $id,
                    'compare' => '='
                ),
            )
        ));
        $posts = $result->posts;
        $count = 0;
        if (!$posts) {
            $count = 0;
        }
        $count = count($posts);

        return $count;
    }

    public static function get_users_contest_entry_id($id)
    {
        $user_id = get_current_user_id();

        $result = new WP_Query(array(
            'author' => $user_id,
            'post_type' => 'contest_entries',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => 'related_contest',
                    'value' =>  $id,
                    'compare' => '='
                ),
            )
        ));
        return $result->posts[0]->ID;
    }

    public static function get_users_contest_entry_first_photo($id)
    {
        $user_id = get_current_user_id();

        $result = new WP_Query(array(
            'author' => $user_id,
            'post_type' => 'contest_entries',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => 'related_contest',
                    'value' =>  $id,
                    'compare' => '='
                ),
            )
        ));
        $entry_id = $result->posts[0]->ID;
        if (have_rows('contest_image_1_group', $entry_id)):
            while (have_rows('contest_image_1_group', $entry_id)): the_row();
                $image = get_sub_field('contest_image_1', $entry_id)['sizes']['large'];
            endwhile;
        endif;
        return $image;
    }

    public static function get_users_open_contest_entries()
    {
        $user_id = get_current_user_id();
        $args = array(
            'author' => $user_id,
            'post_type' => 'contest_entries',
            'posts_per_page' => -1,
        );
        return new WP_Query($args);
    }

    public static function get_users_content_entry_data($entry_id)
    {
        $images = array();

        for ($i = 0; $i <= 4; $i++) :
            while (have_rows('contest_image_' . $i . '_group', $entry_id)): the_row();
                $images[$i]['image'] = get_sub_field('contest_image_' . $i, $entry_id);
                $images[$i]['vote_count'] = get_sub_field('vote_count_' . $i, $entry_id);
                $images[$i]['priority'] = get_sub_field('photo_priority_' . $i, $entry_id);
            endwhile;
        endfor;

        $post_author_ID = get_post($entry_id)->post_author;

        $result =
            [
                'contest_id' => get_post_meta($entry_id, 'related_contest', true),
                'entry_id' => $entry_id,
                'author' => $post_author_ID,
                'images' => $images,
                'total_votes' => array_sum(array_column($images, 'vote_count')),
            ];

        return $result;
    }

    public static function get_users_past_contests()
    {
        $user_id = get_current_user_id();
        $args = array(
            'author' => $user_id,
            'post_type' => 'contest_entries',
            'posts_per_page' => -1,

        );
        $posts = new WP_Query($args);

        $contest_ids = array();
        foreach ($posts->posts as $index => $post) {
            $contest_ids[$index] = get_post_meta($post->ID, 'related_contest', true);
        }

        $contests_args = array(
            'post_type' => 'contests',
            'posts_per_page' => -1,
            'post__in' => $contest_ids,
            'tax_query' => array(
                array(
                    'taxonomy' => 'contest_status',
                    'field' => 'slug',
                    'terms' => 'closed-contests',
                )
            ),
        );
        return new WP_Query($contests_args);
    }

    public function charge_exposure_levels()
    {
        if ($_POST['charge_exposure_levels_nonce']) {
            $entry_id = $_POST['contest_entry_id'];
            if (!empty($entry_id)) {
                for ($i = 1; $i <= 4; $i++) {
                    update_post_meta($entry_id, 'contest_image_' . $i . '_group_photo_priority_' . $i, 1440); //1440 = 24h in minutes
                }
                $charge_pts = (int)get_user_meta(get_current_user_id(), 'charge_pts', true);
                update_user_meta(get_current_user_id(), 'charge_pts', $charge_pts - 1);
            }
            wp_redirect(get_home_url() . '/' . $_POST['_wp_http_referer']);
            exit;
        }
    }

    public function boost_exposure_level()
    {
        if ($_POST['boost_exposure_level_nonce']) {
            $entry_id = $_POST['contest_entry_id'];
            $photo_id = $_POST['photo_id'];

            if (!empty($entry_id) && !empty($photo_id)) {
                update_post_meta($entry_id, 'contest_image_' . $photo_id . '_group_photo_priority_' . $photo_id, 1440); //1440 = 24h in minutes
                $boost_pts = (int)get_user_meta(get_current_user_id(), 'boost_pts', true);
                update_user_meta(get_current_user_id(), 'boost_pts', $boost_pts - 1);
            }
            wp_redirect(get_home_url() . '/' . $_POST['_wp_http_referer']);
            exit;
        }
    }

    public static function set_exposure_levels()
    {
        $result = new WP_Query(array(
            'post_status' => 'publish',
            'post_type' => 'contest_entries',
            'posts_per_page' => -1,
        ));

        $posts = $result->posts;
        foreach ($posts as $post) {
            $id = $post->ID;
            for ($i = 1; $i <= 4; $i++) {
                $exposure_level = get_post_meta($id, 'contest_image_' . $i . '_group_photo_priority_' . $i, true);
                $new_exposure_level = $exposure_level;
                if ($exposure_level >= 5 ) {
                    $new_exposure_level = (int) $exposure_level - 5;
                } elseif ($exposure_level == 4 ) {
                    $new_exposure_level = (int) $exposure_level - 4;
                } elseif ($exposure_level == 3 ) {
                    $new_exposure_level = (int) $exposure_level - 3;
                } elseif ($exposure_level == 2 ) {
                    $new_exposure_level = (int) $exposure_level - 2;
                } elseif ($exposure_level == 1 ) {
                    $new_exposure_level = (int) $exposure_level - 1;
                }
                update_post_meta($id, 'contest_image_' . $i . '_group_photo_priority_' . $i, $new_exposure_level);
            }
        }
    }

    function gf_custom_cron_func() {
         $this->set_exposure_levels();
    }

}