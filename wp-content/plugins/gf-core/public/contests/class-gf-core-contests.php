<?php
/**
 * GF Core - Contests CPT
 */

class GFCore_Contests
{

    /**
     * Initialize the class and set its properties.
     *
     * @param string $plugin_name The name of this plugin.
     * @param string $version The version of this plugin.
     * @since    1.0.0
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    function contests_cpt_init()
    {
        $labels = array(
            'name' => _x('Contests', 'post type general name', 'theme core'),
            'singular_name' => _x('Contest', 'post type singular name', 'theme core'),
            'menu_name' => _x('Contests', 'admin menu', 'theme core'),
            'name_admin_bar' => _x('Contests', 'add new on admin bar', 'theme core'),
            'add_new' => _x('Add new Contest', 'theme core'),
            'add_new_item' => __('Add new Contest', 'theme core'),
            'new_item' => __('New contest', 'theme core'),
            'edit_item' => __('Edit contest', 'theme core'),
            'view_item' => __('See contest', 'theme core'),
            'all_items' => __('All contests', 'theme core'),
            'search_items' => __('Search contests:', 'theme core'),
            'not_found' => __('No contests found', 'theme core'),
            'not_found_in_trash' => __('No contests found in trash', 'theme core')
        );
        $args = array(
            'labels' => $labels,
            'description' => __('Description', 'theme core'),
            'menu_icon' => 'dashicons-awards',
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'contest'),
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => 6,
            'show_in_rest' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'page-attributes'),
            'taxonomies' => false
        );
        register_post_type('contests', $args);
    }

    function create_contest_status_taxonomy()
    {

        $labels = array(
            'name' => _x('Contest Status', 'taxonomy general name'),
            'singular_name' => _x('Contest Status', 'taxonomy singular name'),
            'search_items' => __('Search Contest Statuses'),
            'all_items' => __('All Contest Statuses'),
            'parent_item' => __('Parent Contest Status'),
            'parent_item_colon' => __('Parent Contest Status:'),
            'edit_item' => __('Edit Contest Status'),
            'update_item' => __('Update Contest Status'),
            'add_new_item' => __('Add New Contest Status'),
            'new_item_name' => __('New Contest Status Name'),
            'menu_name' => __('Contest Status'),
        );

        register_taxonomy('contest_status', array('contests'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'contests'),
            'show_in_rest' => true
        ));
    }

    function create_contest_type_taxonomy()
    {

        $labels = array(
            'name' => _x('Contest Types', 'taxonomy general name'),
            'singular_name' => _x('Contest Type', 'taxonomy singular name'),
            'search_items' => __('Search Contest Types'),
            'all_items' => __('All Contest Types'),
            'parent_item' => __('Parent Contest Type'),
            'parent_item_colon' => __('Parent Contest Types:'),
            'edit_item' => __('Edit Contest Type'),
            'update_item' => __('Update Contest Types'),
            'add_new_item' => __('Add New Contest Type'),
            'new_item_name' => __('New Contest Type Name'),
            'menu_name' => __('Contest Type'),
        );

        register_taxonomy('contest_type', array('contests'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'show_in_rest' => true,
            'rewrite' => array('slug' => 'contest-types')
        ));
    }

    function restrict_contests()
    {
        global $post;

        if ($post->post_type == 'contests' || $post->post_type == 'contest_entries' || $post->post_type == 'votes' || is_page('contest-vote')) {
            if (!is_user_logged_in()) {
                wp_redirect('/login');
                exit;
            }
        }
    }

    public static function get_badge_id_by_slug($slug)
    {
        $badges = new WP_Query(array(
            'post_type' => 'mycred_badge',
            'name' => $slug,
            'numberposts' => 1
        ));

        return $badges->posts[0]->ID;

    }

    /**
     * Update users vote power by a certain amount (max 12)
     * @param $user_id
     * @param $amount
     */
    public function update_user_vote_power($user_id, $amount) {
        $amount_int = (int) $amount;
        $users_vote_power = (int) get_user_meta($user_id, 'vote_power');

        if ($users_vote_power <= 12) {
            update_user_meta($user_id, 'vote_power', $users_vote_power + $amount_int);
        }
    }

    /**
     * Update users special points by type
     * @param $user_id
     * @param $amount
     */
    public function update_user_special_points($user_id, $amount, $type) {
        $amount_int = (int) $amount;
        $users_vote_power = (int) get_user_meta($user_id, $type);

        if ($users_vote_power <= 12) {
            update_user_meta($user_id, $type, $users_vote_power + $amount_int);
        }
    }

    public function update_user_points($user_id, $amount)
    {
        $amount_int = (int) $amount;
        $user_pts = get_user_meta($user_id, 'user_pts', true);
        $user_pts = (int) $user_pts + $amount_int;
        update_user_meta($user_id, 'user_pts', $user_pts);
    }

    /**
     * Assign badges to users
     * @param $post_ID
     */
    function distribute_badges($post_ID)
    {
        $winner = GFCore_Votes::get_contest_winners_top($post_ID, 1);
        foreach ($winner as $user) {
            mycred_assign_badge_to_user($user, $this->get_badge_id_by_slug('winner'), 1);
            $this->update_user_vote_power($user, 1);
            $this->update_user_special_points($user, 2, 'trade_pts');
            $this->update_user_special_points($user, 2, 'boost_pts');
            $this->update_user_special_points($user, 2, 'charge_pts');
            $this->update_user_points($user, 1000);
        }

        $second = GFCore_Votes::get_contest_winners_top($post_ID, 2)[1];
            mycred_assign_badge_to_user($second, $this->get_badge_id_by_slug('second'), 1);
            $this->update_user_vote_power($second, 1);
            $this->update_user_special_points($user, 1, 'trade_pts');
            $this->update_user_special_points($user, 1, 'boost_pts');
            $this->update_user_special_points($user, 1, 'charge_pts');
            $this->update_user_points($second, 500);

        $third = GFCore_Votes::get_contest_winners_top($post_ID, 3)[2];
        mycred_assign_badge_to_user($third, $this->get_badge_id_by_slug('third'), 1);
        $this->update_user_vote_power($third, 1);
        $this->update_user_special_points($user, 1, 'trade_pts');
        $this->update_user_special_points($user, 1, 'boost_pts');
        $this->update_user_special_points($user, 1, 'charge_pts');
        $this->update_user_points($third, 250);

        $top10 = GFCore_Votes::get_contest_winners_top($post_ID, 10);
        foreach ($top10 as $user) {
            mycred_assign_badge_to_user($user, $this->get_badge_id_by_slug('top-10'), 1);
            $this->update_user_vote_power($user, 1);
            $this->update_user_special_points($user, 3, 'trade_pts');
            $this->update_user_special_points($user, 3, 'boost_pts');
            $this->update_user_special_points($user, 3, 'charge_pts');
            $this->update_user_points($user, 200);
        }

        $top100 = GFCore_Votes::get_contest_winners_top($post_ID, 100);
        foreach ($top100 as $user) {
            mycred_assign_badge_to_user($user, $this->get_badge_id_by_slug('top-100'), 1);
            $this->update_user_vote_power($user, 1);
            $this->update_user_special_points($user, 2, 'trade_pts');
            $this->update_user_special_points($user, 2, 'boost_pts');
            $this->update_user_special_points($user, 2, 'charge_pts');
            $this->update_user_points($user, 100);
        }

        $top10perc = GFCore_Votes::get_contest_winners_top_percentage($post_ID, 10);
        foreach ($top10perc as $user) {
            mycred_assign_badge_to_user($user, $this->get_badge_id_by_slug('top-10-percent'), 1);
            $this->update_user_vote_power($user, 1);
            $this->update_user_special_points($user, 1, 'trade_pts');
            $this->update_user_special_points($user, 1, 'boost_pts');
            $this->update_user_special_points($user, 1, 'charge_pts');
            $this->update_user_points($user, 50);
        }

        $top20perc = GFCore_Votes::get_contest_winners_top_percentage($post_ID, 20);
        foreach ($top20perc as $user) {
            mycred_assign_badge_to_user($user, $this->get_badge_id_by_slug('top-20-percent'), 1);
            $this->update_user_vote_power($user, 1);
            $this->update_user_special_points($user, 1, 'trade_pts');
            $this->update_user_special_points($user, 1, 'boost_pts');
            $this->update_user_special_points($user, 1, 'charge_pts');
            $this->update_user_points($user, 10);
        }

    }

    //Set post_status based on the current date
    function set_contest_status($post_ID)
    {

        $post_type = get_post_type($post_ID);

        if ($post_type == 'contests') {

            $start = strtotime(get_field('contest_starting_time_and_date', $post_ID));
            $end = strtotime(get_field('contest_end_time_and_date', $post_ID));
            $now = current_time( 'timestamp' );
            $status = '';

            if ($start && $end) {
                if (($start <= $now) && ($end >= $now)) {
                    $status = 'open-contests';
                } elseif ($start >= $now) {
                    $status = 'upcoming-contests';
                } elseif ($end < $now) {
                    $status = 'closed-contests';
                }
            } elseif ($start) {
                if ($start <= $now) {
                    $status = 'close-contests';
                } elseif ($start >= $now) {
                    $status = 'upcoming-contests';
                } elseif ($start == $now) {
                    $status = 'open-contests';
                }
            } else {
                $status = 'closed-contests';
            }

            if ($status === 'closed-contests') {
                $this->distribute_badges($post_ID);
            }

            wp_set_object_terms($post_ID, $status, 'contest_status');
            wp_set_object_terms($post_ID, get_the_title($post_ID), 'contest');

            return $post_ID;
        }
    }

    function add_acf_columns($columns)
    {
        return array_merge($columns, array(
            'contest_starting_time_and_date' => __('Contest Start'),
            'contest_end_time_and_date' => __('Contest End')
        ));
    }

    function contests_custom_column($column, $post_id)
    {
        switch ($column) {
            case 'contest_starting_time_and_date':
                echo get_field('contest_starting_time_and_date', $post_id);
                break;
            case 'contest_end_time_and_date':
                echo get_field('contest_end_time_and_date', $post_id);
                break;
        }
    }

    function custom_schedule($schedules)
    {
        $schedules['every_1m'] = array('interval' => 60, 'display' => 'Once every minute');
        $schedules['every_5m'] = array('interval' => 300, 'display' => 'Every 5 minutes');
        return $schedules;
    }

    public static function get_stunning_header_background_url($post_id)
    {
        $image = unserialize(get_post_meta($post_id)['fw_options'][0]);
        $image = $image['header-stunning-customize']['yes']['header-stunning-customize-styles']['yes']['header-stunning-styles-popup']['stunning_bg_image']['data']['css']['background-image'];
        $image = str_replace(array('("', '")'), '', $image);
        return str_replace(array('url'), '', $image);
    }

    public function gf_contest_status_cron_func() {
        $this->trigger_post_status_check();
    }

    public function trigger_post_status_check() {
        $result = new WP_Query(array(
            'post_status' => 'publish',
            'post_type' => 'contests',
            'posts_per_page' => -1,
        ));
        $posts = $result->posts;

        foreach ($posts as $post) {
            $post_id = $post->ID;
            $this->set_contest_status($post_id);
        }
    }

    public static function is_active_contest($post) {
        return has_term( 'open-contests', 'contest_status', $post );
    }

}