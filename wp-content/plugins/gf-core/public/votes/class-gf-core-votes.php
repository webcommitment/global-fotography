<?php

/**
 * GF Core - Votes CPT
 */

class GFCore_Votes
{

    /**
     * Initialize the class and set its properties.
     *
     * @param string $plugin_name The name of this plugin.
     * @param string $version The version of this plugin.
     * @since    1.0.0
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    function votes_cpt_init()
    {
        $labels = array(
            'name' => _x('Votes', 'post type general name', 'theme core'),
            'singular_name' => _x('Vote', 'post type singular name', 'theme core'),
            'menu_name' => _x('Votes', 'admin menu', 'theme core'),
            'name_admin_bar' => _x('Votes', 'add new on admin bar', 'theme core'),
            'add_new' => _x('Create new', 'theme core'),
            'add_new_item' => __('Add new vote', 'theme core'),
            'new_item' => __('New vote', 'theme core'),
            'edit_item' => __('Edit vote', 'theme core'),
            'view_item' => __('See vote', 'theme core'),
            'all_items' => __('Contest Votes', 'theme core'),
            'search_items' => __('Search votes:', 'theme core'),
            'not_found' => __('No votes found', 'theme core'),
            'not_found_in_trash' => __('No votes found in trash', 'theme core')
        );
        $args = array(
            'labels' => $labels,
            'description' => __('Description', 'theme core'),
            'menu_icon' => 'dashicons-awards',
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => 'edit.php?post_type=contests',
            'query_var' => true,
            'rewrite' => array('slug' => 'vote'),
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => 6,
            'show_in_rest' => true,
            'supports' => array('title', 'page-attributes', 'author'),
            'taxonomies' => false
        );
        register_post_type('votes', $args);
    }

    /**
     * Pre set the related contest item value
     * @param $field
     * @return mixed
     */
    function my_acf_prepare_field($field)
    {
        $post = get_post($_GET['contest']);

        if (is_admin()) {
            return $field;
        }

        if (isset($post)) {
            $post_id = $post->ID;
            $field['value'] = $post_id;
        }
        return $field;

    }

    /**
     * Create new contest entry with related contest content
     * @param $post_id
     */
    function votes_pre_save_post($post_id)
    {
        $post_type = get_post_type($post_id);

        if ($post_type == 'votes') {

            if (!is_admin()) {
                $current_user = wp_get_current_user();
                $contest = get_field('contest_item', $post_id);
                // Set the post data
                $new_post = array(
                    'ID' => $post_id,
                    'post_title' => $contest->post_title . '-' . $current_user->user_login . '-' . current_time('timestamp'),
                );

                $votes = $_POST['acf']['field_5fbf5bf91d292'];
                $user_id = get_current_user_id();

                foreach ($votes as $vote) {
                    $contest_entry_id = (int)$vote['field_5ff2d7e476ae3'];
                    $contest_entry_row = $vote['field_5ff2ea051c302'];
                    $vote_count = get_user_meta($user_id, 'vote_power', true);
                    $vote_count = (int)$vote_count;
                    $count = get_post_meta($contest_entry_id, 'contest_image_' . $contest_entry_row . '_group_vote_count_' . $contest_entry_row, true);
                    $count = (int)$count;
                    $count = $count + $vote_count;

                    $meta_key = 'contest_image_' . $contest_entry_row . '_group_vote_count_' . $contest_entry_row;

                    update_post_meta($contest_entry_id, $meta_key, $count);
                }

                // Remove the hook to avoid infinite loop. Please make sure that it has
                // the same priority (20)
                remove_action('acf/save_post', 'my_save_post', 20);
                // Update the post
                wp_update_post($new_post);
                // Add the hook back
                add_action('acf/save_post', 'my_save_post', 20);

            }
        } else {
            return $post_id;
        }
    }

    public static function already_voted($contest_id)
    {
        $user_id = get_current_user_id();

        $result = new WP_Query(array(
            'author' => $user_id,
            'post_type' => 'votes',
            'post_status' => 'publish',
            'posts_per_page' => 1,
            'meta_query' => array(
                array(
                    'key' => 'related_contest',
                    'value' => array($contest_id),
                    'compare' => 'IN'
                )
            )
        ));
        $count = 0;

        $arr = $result->posts[0];
        if (!empty($arr)) {
            $count = 1;
        }

        return $count;
    }

    public static function get_contest_results($contest_id)
    {

        $entries = new WP_Query(array(
            'post_type' => 'contest_entries',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => 'related_contest',
                    'value' => array($contest_id),
                    'compare' => 'IN'
                )
            )
        ));

        $results = array();

        foreach ($entries->posts as $index => $entry) {
            $images = array();

            for ($i = 0; $i <= 4; $i++) :
                while (have_rows('contest_image_' . $i . '_group', $entry->ID)): the_row();
                    $images[$i]['image'] = get_sub_field('contest_image_' . $i, $entry->ID);
                    $images[$i]['vote_count'] = get_sub_field('vote_count_' . $i, $entry->ID);
                    $images[$i]['priority'] = get_sub_field('photo_priority_' . $i, $entry->ID);
                endwhile;
            endfor;

            $results[$index] =
                [
                    'contest_id' => get_post_meta($entry->ID, 'related_contest', true),
                    'entry_id' => $entry->ID,
                    'author' => $entry->post_author,
                    'images' => $images,
                    'total_votes' => array_sum(array_column($images, 'vote_count')),
                ];
        }


        usort($results, function ($a, $b) {
            if ($a['total_votes'] == $b['total_votes'])
                return 0;
            else if ($a['total_votes'] > $b['total_votes'])
                return 1;
            else
                return -1;
        });

        return array_reverse($results);
    }

    public static function get_users_total_votes($user_ID)
    {
        $entries = new WP_Query(array(
            'author'            => $user_ID,
            'post_type'         => 'contest_entries',
            'post_status'       => 'publish',
            'posts_per_page'    => -1,
        ));

        $results = array();

        foreach ($entries->posts as $index => $entry) :
            $images = array();
            for ($i = 0; $i <= 4; $i++) :
                while (have_rows('contest_image_' . $i . '_group', $entry->ID)): the_row();
                    $images[$i]['vote_count'] = get_sub_field('vote_count_' . $i, $entry->ID);
                endwhile;
            endfor;
            $results[$index] = [
                'total_votes' => array_sum(array_column($images, 'vote_count')),
            ];
        endforeach;

        return array_sum(array_column($results, 'total_votes'));
    }

    /**
     * Get contest top (x number) by post_id
     * @param $post_id
     * @param $top_count
     * @return array
     */
    public static function get_contest_winners_top($post_id, $top_count)
    {
        $results = GFCore_Votes::get_contest_results($post_id);
        $users = array();

        for ($i = 1; $i <= $top_count; $i++) {
            $users[$i] = $results[$i]['author'];
        }
        return $users;
    }

    /**
     * Get contest top percentage (x number) by post_id
     * @param $post_id
     * @param $top_perc
     * @return array
     */
    public static function get_contest_winners_top_percentage($post_id, $top_perc)
    {
        $results = GFCore_Votes::get_contest_results($post_id);
        $users = array();

        $total_items = count($results);
        $number_of_users = round(($top_perc / 100) * $total_items);

        for ($i = 1; $i <= $number_of_users; $i++) {
            $users[$i] = $results[$i]['author'];
        }
        return $users;
    }
}

?>