<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    GF_Core
 * @subpackage GF_Core/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    GF_Core
 * @subpackage GF_Core/includes
 * @author     Your Name <email@example.com>
 */
class GF_Core
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      GF_Core_Loader $loader Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $gf_core The string used to uniquely identify this plugin.
     */
    protected $gf_core;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $version The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
        if (defined('GF_CORE_VERSION')) {
            $this->version = GF_CORE_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->gf_core = 'gf-core';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();

    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - GF_Core_Loader. Orchestrates the hooks of the plugin.
     * - GF_Core_i18n. Defines internationalization functionality.
     * - GF_Core_Admin. Defines all hooks for the admin area.
     * - GF_Core_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies()
    {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-gf-core-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-gf-core-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-gf-core-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-gf-core-public.php';

        /**
         * Custom GF Core plugin partials
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/contests/class-gf-core-contests.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/contest-entries/class-gf-core-contest-entries.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/votes/class-gf-core-votes.php';

        $this->loader = new GF_Core_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the GF_Core_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale()
    {

        $plugin_i18n = new GF_Core_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks()
    {

        $plugin_admin = new GF_Core_Admin($this->get_gf_core(), $this->get_version());

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks()
    {

        $plugin_public = new GF_Core_Public($this->get_gf_core(), $this->get_version());

        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');
        $this->loader->add_action('init', $plugin_public, 'add_custom_options_page');
        $this->loader->add_action( 'woocommerce_order_status_completed', $plugin_public, 'update_user_point_balance', 10, 2 );
        $this->loader->add_action( 'woocommerce_order_status_completed', $plugin_public, 'update_unlocked_contests', 10, 2 );
        $this->loader->add_filter( 'mycred_all_references', $plugin_public,  'mycred_custom_references' );
        $this->loader->add_action('user_register', $plugin_public, 'set_users_balances');

        /*
         * GF Core Contests
         * */
        $plugin_gfcore_contests = new GFCore_Contests($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('init', $plugin_gfcore_contests, 'contests_cpt_init');
        $this->loader->add_action('init', $plugin_gfcore_contests, 'create_contest_status_taxonomy', 0);
        $this->loader->add_action('init', $plugin_gfcore_contests, 'create_contest_type_taxonomy', 0);
        $this->loader->add_filter('wp', $plugin_gfcore_contests, 'restrict_contests', 0);
        $this->loader->add_action('save_post', $plugin_gfcore_contests, 'set_contest_status');
        $this->loader->add_filter('cron_schedules', $plugin_gfcore_contests, 'custom_schedule');
        $this->loader->add_filter('manage_edit-contests_columns', $plugin_gfcore_contests, 'add_acf_columns');
        $this->loader->add_action('manage_contests_posts_custom_column', $plugin_gfcore_contests, 'contests_custom_column', 10, 2);

        $this->loader->add_action('check_contest_status', $plugin_gfcore_contests, 'set_contest_status');
        $this->loader->add_action('gf_contest_status_cron', $plugin_gfcore_contests, 'gf_contest_status_cron_func');

        /*
         * GF Core Contest entries
         * */
        $plugin_gfcore_contest_entries = new GFCore_Contest_Entries($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('init', $plugin_gfcore_contest_entries, 'contest_entries_cpt_init');
        $this->loader->add_action('init', $plugin_gfcore_contest_entries, 'create_contest_taxonomy');
        $this->loader->add_filter('manage_edit-contest_entries_columns', $plugin_gfcore_contest_entries, 'add_acf_columns');
        $this->loader->add_action('manage_contest_entries_posts_custom_column', $plugin_gfcore_contest_entries, 'contest_entries_custom_column', 10, 2);

        $this->loader->add_filter('posts_where', $plugin_gfcore_contest_entries, 'contest_entries_attachments_wpquery_where');
        $this->loader->add_filter('acf/save_post', $plugin_gfcore_contest_entries, 'contest_entries_save_post', 10,1);
        //$this->loader->add_filter('acf/save_post', $plugin_gfcore_contest_entries, 'contest_entries_save_post', 20,1);
        $this->loader->add_filter('acf/prepare_field/name=related_contest', $plugin_gfcore_contest_entries, 'my_acf_prepare_field');

        $this->loader->add_action('admin_post_voting_form', $plugin_gfcore_contest_entries, 'prefix_admin_voting_form_callback');
        $this->loader->add_action('admin_post_nopriv_voting_form', $plugin_gfcore_contest_entries, 'prefix_admin_voting_form_callback');

        $this->loader->add_action('init', $plugin_gfcore_contest_entries, 'charge_exposure_levels');
        $this->loader->add_action('init', $plugin_gfcore_contest_entries, 'boost_exposure_level');

        $this->loader->add_action('gf_custom_cron', $plugin_gfcore_contest_entries, 'gf_custom_cron_func');

        /*
        * GF Core Votes
        * */
        $plugin_gfcore_votes = new GFCore_Votes($this->get_plugin_name(), $this->get_version());
        $this->loader->add_action('init', $plugin_gfcore_votes, 'votes_cpt_init');
        $this->loader->add_filter('acf/prepare_field/name=contest_item', $plugin_gfcore_votes, 'my_acf_prepare_field');
        $this->loader->add_filter('acf/save_post', $plugin_gfcore_votes, 'votes_pre_save_post', 20);
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @return    string    The name of the plugin.
     * @since     1.0.0
     */
    public function get_gf_core()
    {
        return $this->gf_core;
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @return    string    The name of the plugin.
     * @since     1.0.0
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @return    GF_Core_Loader    Orchestrates the hooks of the plugin.
     * @since     1.0.0
     */
    public function get_loader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @return    string    The version number of the plugin.
     * @since     1.0.0
     */
    public function get_version()
    {
        return $this->version;
    }

}
