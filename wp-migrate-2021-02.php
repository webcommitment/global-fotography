<?php
require_once('wp-load.php');

/**
 * v1.0.1 Migration - Create user meta for already created users
 */
$users = get_users();
foreach($users as $user) {
    $user_id = $user->ID;
    $vote_power = get_user_meta($user_id, 'vote_power', true);
    $user_pts = get_user_meta($user_id, 'user_pts', true);
    $trade_pts = get_user_meta($user_id, 'trade_pts', true);
    $charge_pts = get_user_meta($user_id, 'charge_pts', true);
    $boost_pts = get_user_meta($user_id, 'boost_pts', true);

    if (!$vote_power) {
        update_user_meta($user_id, 'vote_power', 1);
        print_r('vote_power created for user with id:' . $user_id . '</br>');
    }
    if (!$user_pts) {
        update_user_meta($user_id, 'user_pts', 0);
        print_r('user_pts created for user with id:' . $user_id . '</br>');
    }
    if (!$trade_pts) {
        update_user_meta($user_id, 'trade_pts', 10);
        print_r('trade_pts created for user with id:' . $user_id . '</br>');
    }
    if (!$charge_pts) {
        update_user_meta($user_id, 'charge_pts', 10);
        print_r('charge_pts created for user with id:' . $user_id . '</br>');
    }
    if (!$boost_pts) {
        update_user_meta($user_id, 'boost_pts', 10);
        print_r('boost_pts created for user with id:' . $user_id . '</br>');
    }
}
?>