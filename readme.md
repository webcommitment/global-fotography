# Global Fotography Documentation
Globalfotography.com is a photography community and game. Users are allowed to take part in online **Contests** created by site admins. Every user can create one **Contest Entry** per **Contest** consisting of max. 4 photos.  

###Contest
Contest is a post created by site admin. Every contest has a title, start and end date, contest details, contest prizes, contest rules and a **Contest Type** (Free, Coupon, Gallery or Prize).The related contest product should be set for every **Contest Type** except Free Contest Type. The **Contest Status** taxonomy is set automatically by a cronjob accordingly to the **Contest** start and end date.

###Contest entries
On **Contest Entry** creation every photo gets it's own set of meta fields like:
* Vote count - a sum of all votes
* Photo priority (exposure) - a maximum amount of 1440 (1440 stands for 24h in minutes), decreased every minute by a cron job. Participants can boost their photos priorities by using a **Boost** or a **Charge**.   

###Contest votes
Every contest participator is allowed to vote unlimited number of times on **Contest entry** photos but can vote only once on a certain photo. Participator can't vote on it's own photos. Users can vote for the photo's on the Vote Page. The photos on the vote page are sorted by their **Vote count** + **Photo priority (exposure)** counts.  

###Contest Configuration
Site admin can create a Winners (globalfotography.com/winners) and Global Pro Picks (globalfotography.com/global-pro-picks) galleries. Besides that admin can configurate the content of the "Boosters modals" (Trade, Charge and Boost) and select the related products.
The "Booster modals" are located on the contests pages.

###Boosters
Every user gets 10 free Boost, Charge and Trade points on account creation. They can also buy boosters through booster modals or win them in paid contests.

###Contest prizes
Contest prizes could be a physical product (camera) or a bundle of boosters. 
